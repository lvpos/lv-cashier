
angular.module('cssApp', ['ionic', 'ngSanitize', 'ngResource', 'ngAnimate', 'ngCordova', 'ngMaterial',
                          /*'angular-hidScanner', 'barcodeListener', */'angular-md5', 'utf8-base64', 'ngCordovaBluetoothLE', 'ng.deviceDetector',
                          'cc.ctrl', 'dd.directives', 'ff.filters', 'ss.services', 'ss.printer'])

.run(['$ionicPlatform', '$ionicHistory', '$rootScope', '$location', '$ionicLoading', '$cordovaSQLite', '$cordovaNetwork', '$cordovaBluetoothLE', 'sessionService',
      function($ionicPlatform, $ionicHistory, $rootScope, $location, $ionicLoading, $cordovaSQLite, $cordovaNetwork, $cordovaBluetoothLE, sessionService) {

    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          
            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
        
        wd = $('body').width();
        if(window.isTablet || wd >= 400) {
            screen.unlockOrientation();
        }else{
            screen.lockOrientation('portrait');
        } 
        
        ///* Database */
        //db = $cordovaSQLite.openDB("cssDB");
        //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS css_data (id integer primary key, data_name varchar, data_value text)");
        
        var type = $cordovaNetwork.getNetwork()
        var isOnline = $cordovaNetwork.isOnline()
        var isOffline = $cordovaNetwork.isOffline()
        
        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            var onlineState = networkState;
            console.log(onlineState);
        })
        
        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            var offlineState = networkState;
            console.log(offlineState);
        })
    });
    
    $ionicPlatform.registerBackButtonAction(function(e) {
        if ($rootScope.backButtonPressedOnceToExit) {
            console.log(0);
            ionic.Platform.exitApp();
        } else if ($ionicHistory.backView()) {
            console.log(1);
            $ionicHistory.goBack();
        } else {
            console.log(2);
            $rootScope.backButtonPressedOnceToExit = true;
            window.plugins.toast.showShortCenter(
                "Press back button again to exit",function(a){},function(b){}
            );
            setTimeout(function(){
                $rootScope.backButtonPressedOnceToExit = false;
            },2000);
        }
        e.preventDefault();
        return false;
    }, 101);
    
    /* Loading Show */

    $rootScope.$on('loading:show', function() {
        if ($location.path() == '/login' || $location.path() == '/forgot' || $location.path() == '/outlets') {
            $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'})
        }
    })

    $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide()
    })
    
    /* Permission Route */
    
    var routeDash = ['/dashboard'];
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        /*if ($location.path() !== '/login') {
            var connected = sessionService.checklog();
        
            connected.then(function(msg) {
                if(msg.data.status > 0)
                {
                    if($location.path() == '/login')
                    {
                        $location.path(routeDash);
                    }
                } else {
                    if($location.path() !== '/forgot')
                    {
                        $location.path('login');
                    }
                }
            })
        }*/
        
        if (sessionService.get('uid_css') == null) {
            if($location.path() !== '/forgot')
            {
                $location.path('login');
            }
        } else {
            if($location.path() == '/login')
            {
                $location.path(routeDash);
            }
            
            if($location.path() == '/outlets' && sessionService.get('enableoutlet') == 'true')
            {
                $location.path(routeDash);
            }
        }
    });  
}])

.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', '$mdGestureProvider', '$mdDateLocaleProvider',
         function($httpProvider, $stateProvider, $urlRouterProvider, $mdGestureProvider, $mdDateLocaleProvider) {
    
    $mdGestureProvider.skipClickHijack();
    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('DD-MM-YYYY');
    };
    
    /* Loading Show Push */

    $httpProvider.interceptors.push(function($rootScope) {
        return {
            request: function(config) {
                $rootScope.$broadcast('loading:show')
                return config
            },
            response: function(response) {
                $rootScope.$broadcast('loading:hide')
                return response
            }
        }
    })

    /* Route List */

    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: 'templates/v1/login.html',
        controller: 'loginCtrl'
    })
    .state('forgot', {
        url: '/forgot',
        templateUrl: 'templates/v1/forgot.html',
        controller: 'loginCtrl'
    })
    .state('outlets', {
        url: '/outlets',
        templateUrl: 'templates/v1/outlets.html',
        controller: 'outletsCtrl'
    })
    .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'templates/v1/dashboard.html',
        controller: 'appCtrl'
    })
    .state('activity', {
        url: '/activity',
        templateUrl: 'templates/v1/activity/main.html',
        controller: 'activityCtrl'
    })
    .state('summary', {
        url: '/summary',
        templateUrl: 'templates/v1/activity/main.html',
        controller: 'activityCtrl'
    })
    .state('receipt', {
        url: '/receipt/:id',
        templateUrl: 'templates/v1/activity/main.html',
        controller: 'activityCtrl'
    })
    .state('setting', {
        url: '/setting',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('tools', {
        url: '/tools',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('taxes-gratuity', {
        url: '/taxes-gratuity',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('bluetooth', {
        url: '/bluetooth',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('printer', {
        url: '/printer',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('support', {
        url: '/support',
        templateUrl: 'templates/v1/setting/main.html',
        controller: 'deviceCtrl'
    })
    .state('print', {
        url: '/print',
        templateUrl: 'templates/v1/activity/main.html',
        controller: 'printCtrl'
    })

    /* Default Route */

    $urlRouterProvider.otherwise('/login');
}])