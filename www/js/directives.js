
angular.module('dd.directives', [])

.directive('checkAll', ['$rootScope', '$ionicPopup', '$cordovaNetwork', '$cordovaBluetoothLE', 'Projects', 'AuthenticationService', 'sessionService', 'deviceDetector',
			function($rootScope, $ionicPopup, $cordovaNetwork, $cordovaBluetoothLE, Projects, AuthenticationService, sessionService, deviceDetector) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	//    window.addEventListener('resume', function() {
	//	var xprint = JSON.parse(sessionService.get('printSel'));
	//	window.plugins.starPrinter.checkBarcode(xprint.name, function(error, result){
	//	    if (error) {
	//		console.error(error);
	//	    } else {
	//		console.log(result);
	//	    }
	//	})
	//    }, false);
	    
	    setInterval(function(){
		var blue = JSON.parse(sessionService.get('enableblue'));
		if (blue != undefined && blue == false) {
		    $cordovaBluetoothLE.initialize({request:true}).then(null, function(err) {
			//console.log(err);
		    }, function(obj) {
			if (obj.status != 'disabled') {
			    sessionService.set('enableblue', true);
			}			
		    })
		} else {
		    $cordovaBluetoothLE.initialize({request:false}).then(null, function(err) {
			//console.log(err);
		    }, function(obj) {
			if (obj.status == 'disabled') {
			    sessionService.set('enableblue', false);
			}			
		    })
		}
		
		//var type = $cordovaNetwork.getNetwork()
		//var isOnline = $cordovaNetwork.isOnline()
		//var isOffline = $cordovaNetwork.isOffline()
		
		//var dvdetect = deviceDetector; console.log(dvdetect);
		//if (dvdetect.os == 'android' || dvdetect.os == 'ios' || dvdetect.os == 'windows-phone') {
		    if (!$cordovaNetwork.isOnline()) {
			sessionService.set('enableinet', false);
		    } else {
			sessionService.set('enableinet', true);
		    }
		//} else {
		//    sessionService.set('enableinet', true);
		//}
		
		//var xprint = JSON.parse(sessionService.get('printSel'));
		//window.plugins.starPrinter.checkBarcode(xprint.name, function(error, result){
		//    if (error) {
		//	console.error(error);
		//    } else {
		//	console.log(result);
		//    }
		//})
		
		// Temp Orders
		var tempOrders = function(temp, ss, err) {
		    var temps = JSON.parse(sessionService.get('orders'));
		    
		    if (Object.prototype.toString.call(temps) === '[object Array]') {
			var log = [];
			var i = 0;
			
			angular.forEach(temps, function(val) {
			    if (val.sales_offline_no == temp.sales_offline_no) {
				if (ss == true) {
				    angular.extend(val, {online: 1});
				}
				
				if (err == true) {
				    angular.extend(val, {api: 0});
				} else {
				    angular.extend(val, {api: 1});
				}
				
				console.log('Change');
				console.log(val);
				log[i] = val;
			    } else {
				log[i] = val;
			    }
			    
			    i++;
			})
			
			sessionService.set('orders', JSON.stringify(temps));
		    }
		}
		
		// Proceed Data
		var proData = function(val, xtime) {
		    setTimeout(function() {
			var order = {}
			angular.extend(order, val.user_data);
			angular.extend(order, {sales_data: val});
			delete val.user_data;
			
			console.log('Proceed');
			console.log(val);
			console.log(order.sales_data);
			Projects.curlPost(Projects.zUrl, 'api/sales/create', order).then(function(msg) {
			    var line = msg.data; console.log(msg.status);
			    if(msg.status == 200)
			    {
				tempOrders(val, true, false);
			    }
			}, function(err) {
			    tempOrders(val, false, true);
			})
		    }, xtime);
		}
		
		var acc = sessionService.get('acc_css');
		var inet = JSON.parse(sessionService.get('enableinet'));
		if (inet == true && acc != undefined) {
		    var temps = JSON.parse(sessionService.get('orders'));
		    
		    if (Object.prototype.toString.call(temps) === '[object Array]') {
			
			var xtime = 5000;
			angular.forEach(temps, function(val) {
			    if (val.api == 0 && val.online == 0 || val.api == 1 && val.online == 0) { //if (val.online == 0) {
				//if (val.api == 0) {
				    tempOrders(val);
				    
				    proData(val, xtime);
				    xtime = xtime + 5000;
				//}
			    }
			})
		    }
		}
	    }, 10000)
	}
    }
}])

.directive('selOutlet', ['$interval', function($interval) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.de-outlet').html($(this).html());
	    });
        }
    }
}])

.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
		
		$('.md-subhead').html(element[0].value);
            });
        }
    };
}])

.directive('searchType', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).on('keydown', function(ev) {
		ev.stopPropagation();
	    });
        }
    }
})

.directive('resizeContent', ['$window', function($window) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    //// Content
	    //setTimeout(function(){
		wd = $('body').width();
		xc = $('body').height(); console.log(xc);
		t1 = $('.toolbar1').height();
		if (wd <= 600) {
		    t2 = $('.toolbar2 .tab-button').height();
		} else {
		    t2 = $('.toolbar2 .sw-button').height();
		}
		console.log(xc+'/'+t1+'/'+t2)
		
		xh = xc - (t1 + t2);
		$(elem).css({'min-height': xh+'px', height: xh+'px'});
		if (wd <= 600) {
		    $('.toolbar2').css({'min-height': t2+'px', height: t2+'px'});
		} else {
		    xt2 = t2 + 8;
		    $('.toolbar2').css({'min-height': xt2+'px', height: xt2+'px'});
		}
		
		setTimeout(function(){
		    rc = xh - 20;
		    $('.rc').css({'min-height': rc+'px', height: rc+'px'});
		    
		    // Main Cart
		    lvc = $('.rc').innerHeight();
		    
		    if (scope.path == '/dashboard') {
			lcn = lvc - (99 + 65); 
			$('.lvr-cnt').css({'min-height': lcn+'px', height: lcn+'px'});
			
			lct = (lvc - (99 + 110) - 16);
			$('.lvr-cart').css({'min-height': lct+'px', height: lct+'px'});
		    } else {
			// Content
			lc1 = $('.lvr-tc1').innerHeight();
			lc2 = $('.lvr-tc2').innerHeight();
			lcn = (lc2 == undefined) ? lvc - lc1 : lvc - (lc1 + lc2); 
			$('.lvr-cnt').css({'min-height': lcn+'px', height: lcn+'px'});
			console.log(lcn+'/'+lc1+'/'+lc2);
			
			// Cart
			lt1 = $('.lvr-t1').innerHeight();
			lt2 = $('.lvr-t2').innerHeight();
			lct = ((lvc - (lt1 + lt2)) - 16);
			$('.lvr-cart').css({'min-height': lct+'px', height: lct+'px'});
			console.log(lct+'/'+lt1+'/'+lt2);
		    }
		}, 200)
	    //}, 800)
	    
	    angular.element($window).bind('resize', function() {
		// Content
		wd = $('body').width();
		xc = $('body').height();
		t1 = $('.toolbar1').height();
		if (wd <= 600) {
		    t2 = $('.toolbar2 .tab-button').height();
		} else {
		    t2 = $('.toolbar2 .sw-button').height();
		}
		console.log(xc+'/'+t1+'/'+t2)
		
		xh = xc - (t1 + t2);
		$(elem).css({'min-height': xh+'px', height: xh+'px'});
		if (wd <= 600) {
		    $('.toolbar2').css({'min-height': t2+'px', height: t2+'px'});
		} else {
		    xt2 = t2 + 8;
		    $('.toolbar2').css({'min-height': xt2+'px', height: xt2+'px'});
		}
		
		rc = xh - 20;
		$('.rc').css({'min-height': rc+'px', height: rc+'px'});
		
		//// Library
		//rcs = $('.rc-search').innerHeight();
		//rc = rc - (rcs + 16);
		//$('.rc-sub').css({'min-height': rc+'px', height: rc+'px'});
		
		// Main Cart
		lvc = $('.rc').innerHeight();
		
		// Content
		lc1 = $('.lvr-tc1').innerHeight();
		lc2 = $('.lvr-tc2').innerHeight();
		lcn = (lc2 == undefined) ? lvc - lc1 : lvc - (lc1 + lc2);
		$('.lvr-cnt').css({'min-height': lcn+'px', height: lcn+'px'});
		
		// Main Cart
		lt1 = $('.lvr-t1').innerHeight();
		lt2 = $('.lvr-t2').innerHeight();
		lxh = lvc - (lt1 + lt2);
		lct = lxh - 16;
		
		$('.lvr-cart').css({'min-height': lct+'px', height: lct+'px'});
	    });
        }
    }
}])

.directive('resizeCart', ['$window', function($window) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		// Sidebar Cart
		setTimeout(function(){
		    sdc = $('.sd-cart').innerHeight();
		    sdb = $('.sd-bar-cart').innerHeight();
		    xsd = sdc - sdb;
		    
		    $('.sd-cn-cart').css({'min-height': xsd+'px', height: xsd+'px'});
		    
		    // Main Cart
		    lvc = $('.sd-cn').innerHeight();
		    lt1 = $('.sd-t1').innerHeight();
		    lt2 = $('.sd-t2').innerHeight();
		    lxh = xsd - (lt1 + lt2);
		    lct = lxh - 16;
		    
		    $('.sd-cn').css({'min-height': lct+'px', height: lct+'px'});
		}, 200)
	    });
        }
    }
}])

.directive('showProducts', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.showPackages').hide();
		$('.showFavorite').hide();
		$('.showCprice').hide();
		$('.showProducts').fadeIn(300);
		
		$('.toolbar2 #categories').removeAttr('class');
		$('.toolbar2 #categories').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow wd-100');
		$('.toolbar2 #packages').addClass('btn-blue');
		$('.toolbar2 #favorite').addClass('btn-blue');
		$('.toolbar2 #csprice').addClass('btn-blue');
		
		$('.toolbar2 #swcategories').removeAttr('class');
		$('.toolbar2 #swcategories').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow');
		$('.toolbar2 #swpackages').addClass('btn-blue');
		$('.toolbar2 #swfavorite').addClass('btn-blue');
		$('.toolbar2 #swcsprice').addClass('btn-blue');
	    });
        }
    }
})

.directive('showPackages', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.showProducts').hide();
		$('.showFavorite').hide();
		$('.showCprice').hide();
		$('.showPackages').fadeIn(300);
		
		$('.toolbar2 #packages').removeAttr('class');
		$('.toolbar2 #packages').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow wd-100');
		$('.toolbar2 #categories').addClass('btn-blue');
		$('.toolbar2 #favorite').addClass('btn-blue');
		$('.toolbar2 #csprice').addClass('btn-blue');
		
		$('.toolbar2 #swpackages').removeAttr('class');
		$('.toolbar2 #swpackages').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow');
		$('.toolbar2 #swcategories').addClass('btn-blue');
		$('.toolbar2 #swfavorite').addClass('btn-blue');
		$('.toolbar2 #swcsprice').addClass('btn-blue');
	    });
        }
    }
})

.directive('showFavorite', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.showProducts').hide();
		$('.showPackages').hide();
		$('.showCprice').hide();
		$('.showFavorite').fadeIn(300);
		
		$('.toolbar2 #favorite').removeAttr('class');
		$('.toolbar2 #favorite').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow wd-100');
		$('.toolbar2 #categories').addClass('btn-blue');
		$('.toolbar2 #packages').addClass('btn-blue');
		$('.toolbar2 #csprice').addClass('btn-blue');
		
		$('.toolbar2 #swfavorite').removeAttr('class');
		$('.toolbar2 #swfavorite').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow');
		$('.toolbar2 #swcategories').addClass('btn-blue');
		$('.toolbar2 #swpackages').addClass('btn-blue');
		$('.toolbar2 #swcsprice').addClass('btn-blue');
	    });
        }
    }
})

.directive('showCprice', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.showProducts').hide();
		$('.showPackages').hide();
		$('.showFavorite').hide();
		$('.showCprice').fadeIn(300);
		
		$('.toolbar2 #csprice').removeAttr('class');
		$('.toolbar2 #csprice').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow wd-100');
		$('.toolbar2 #categories').addClass('btn-blue');
		$('.toolbar2 #packages').addClass('btn-blue');
		$('.toolbar2 #favorite').addClass('btn-blue');
		
		$('.toolbar2 #swcsprice').removeAttr('class');
		$('.toolbar2 #swcsprice').addClass('btn btn-primary cl-mg-all cl-btn cl-bod-rad cl-bod-shadow');
		$('.toolbar2 #swcategories').addClass('btn-blue');
		$('.toolbar2 #swpackages').addClass('btn-blue');
		$('.toolbar2 #swfavorite').addClass('btn-blue');
	    });
        }
    }
})

.directive('payCash', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.pay-card').hide();
		$('.pay-form').fadeIn(300);
		$('.pay-cash').fadeIn(300);
	    });
        }
    }
})

.directive('payCard', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.pay-cash').hide();
		$('.pay-form').fadeIn(300);
		$('.pay-card').fadeIn(300);
	    });
        }
    }
})

.directive('stringToNumber', function() {
    return {
	require: 'ngModel',
	link: function(scope, element, attrs, ngModel) {
	    ngModel.$parsers.push(function(value) {
		return '' + value;
	    });
	    ngModel.$formatters.push(function(value) {
		return parseFloat(value, 10);
	    });
	}
    };
})