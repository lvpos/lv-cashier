
angular.module('ff.filters', [])

.filter('searchJoblist', function($filter){
    return function(arr, jobSearch, scope){
        if(!jobSearch) {
            return arr;
        }

        var result = [];
        filter = jobSearch.filter;
        jobs = jobSearch.value;
        if(jobSearch.value !== undefined)
        {
            jobSearch = jobSearch.value.toLowerCase();
        }

        angular.forEach(arr, function(item) {
            if(filter == undefined || filter == '')
            {
                if(item.order_no.toLowerCase().indexOf(jobSearch) !== -1 || item.skey.toLowerCase().indexOf(jobSearch) !== -1 || item.order_status.toLowerCase().indexOf(jobSearch) !== -1 || 
                   item.client.client_name.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_name.toLowerCase().indexOf(jobSearch) !== -1 ||
                   item.order_ori_address.ori_address_0.toLowerCase().indexOf(jobSearch) !== -1 || item.order_ori_address.ori_address_1.toLowerCase().indexOf(jobSearch) !== -1 || item.order_ori_address.ori_address_2.toLowerCase().indexOf(jobSearch) !== -1 ||
                   item.order_dest_address.dest_address_0.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_address.dest_address_1.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_address.dest_address_2.toLowerCase().indexOf(jobSearch) !== -1 ||
                   item.order_ori_region.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_region.toLowerCase().indexOf(jobSearch) !== -1) 
                {
                    result.push(item);
                }
            }
            
            if(filter > 0)
            {
                switch(filter)
                {
                    case '1':
                        if(item.order_no.toLowerCase().indexOf(jobSearch) !== -1 || item.skey.toLowerCase().indexOf(jobSearch) !== -1) {
                            result.push(item);
                        }
                    break;
                    case '2':
                        if(item.client.client_name.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_name.toLowerCase().indexOf(jobSearch) !== -1) {
                            result.push(item);
                        }
                    break;
                    case '3':
                        if(item.order_ori_address.ori_address_0.toLowerCase().indexOf(jobSearch) !== -1 || item.order_ori_address.ori_address_1.toLowerCase().indexOf(jobSearch) !== -1 || item.order_ori_address.ori_address_2.toLowerCase().indexOf(jobSearch) !== -1 ||
                           item.order_dest_address.dest_address_0.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_address.dest_address_1.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_address.dest_address_2.toLowerCase().indexOf(jobSearch) !== -1) {
                            result.push(item);
                        }
                    break;
                    case '4':
                        if(item.order_ori_region.toLowerCase().indexOf(jobSearch) !== -1 || item.order_dest_region.toLowerCase().indexOf(jobSearch) !== -1) {
                            result.push(item);
                        }
                    break;
                    case '5':
                        if(item.order_status.toLowerCase().indexOf(jobSearch) !== -1) {
                            result.push(item);
                        }
                    break;
                }
            }
        });
        
        if(jobSearch.sortby !== undefined)
        {
            var qrResult = jobSearch.sortby;
            var qrSplit = qrResult.split(";");
            var reverse = (qrSplit[1] == 'false') ? false : !reverse;
            console.log(reverse);
               
            if(jobSearch.sortby == '')
            {
                arr = $filter('orderBy')(arr, '', false);
                result = $filter('orderBy')(result, '', false);
            } else {
                arr = $filter('orderBy')(arr, qrSplit[0], reverse);
                result = $filter('orderBy')(result, qrSplit[0], reverse);
            }
        }

        if(result == '')
        {
            if(filter == '' && jobs == undefined || filter != '' && jobs == undefined || filter == undefined && jobs == undefined)
            {
                return arr;
            } else {
                return result;
            }
        } else {
            return result;
        }
    };
})

.filter('searchScanlist', function(){
    return function(arr, jobSearch){
        if(!jobSearch) {
            return arr;
        }

        var result = [];
        filter = jobSearch.filter;
        
        angular.forEach(arr, function(item) {
            if(item.status == filter)
            {
                result.push(item);
            }
        });

        if(result == '')
        {
            if(filter == '' || filter == undefined)
            {
                return arr;
            } else {
                return result; 
            }
        } else {
            return result; 
        }
    };
})