
angular.module('cc.ctrl', [])

// Login
.controller('loginCtrl', ['$scope', '$state', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                          function($scope, $state, $location, Projects, AuthenticationService, sessionService) {

    AuthenticationService.ClearCredentials();
    
    $scope.login = {};
    $scope.loginAct = function(login) {
        setTimeout(function(){
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/apps_login', login).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    sessionService.set('enableblue', false);
                    sessionService.set('enableoutlet', false);
                    sessionService.set('acc_css', JSON.stringify(line.data));
                    sessionService.set('uid_css', line.data.session_key);
                    $state.go('outlets');
                }
            })
        }, 500)
    }
    
    $scope.reg = {};
    $scope.regAct = function(reg) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, '', login).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $state.go('login');
            }
        })
    }
    
    $scope.forgot = {};
    $scope.forgotAct = function(forgot) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, '', login).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $state.go('login');
            }
        })
    }

}])

// Outlets
.controller('outletsCtrl', ['$scope', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                            function($scope, $location, Projects, AuthenticationService, sessionService) {
    
    var dtAcc = JSON.parse(sessionService.get('acc_css'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    $scope.otlData = dtAcc.outlets;
    
    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
    Projects.curlPost(Projects.zUrl, 'api/packages/index/1000', dtAcc).then(function(msg) {
        var line = msg.data;
        if(msg.status == 200)
        {
            sessionService.set('packages', JSON.stringify(line.data));
        }
    })
    
    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
    Projects.curlPost(Projects.zUrl, 'api/home', dtAcc).then(function(msg) {
        var line = msg.data;
        if(msg.status == 200)
        {
            sessionService.set('roles', JSON.stringify(line.data));
        }
    })
    
    $scope.checkAct = function(pp) {
        var status = 0;
        angular.forEach(dtAcc.outlets, function(val) {
            if (val.outlet_id == pp.outlet_id) {
                status = 1;
                
                sessionService.set('enableoutlet', true);
                sessionService.set('favorite', JSON.stringify(val.favorite_products));
                sessionService.set('categories', JSON.stringify(val.categories));
                
                delete val.favorite_products;
                delete val.categories;
                sessionService.set('outlet', JSON.stringify(val));
                
                delete dtAcc.outlets;
                angular.extend(dtAcc, {outlet_id: pp.outlet_id});
                sessionService.set('acc_css', JSON.stringify(dtAcc));
            }
        })
        
        if (status > 0) {
            $location.path('cashier');
        }
    }
    
}])

.controller('logCtrl', ['$scope', '$location', 'sessionService', function($scope, $location, sessionService) {
    $scope.logout = function() {
        sessionService.destroy('uid_css');
        sessionService.destroy('acc_css');
        sessionService.destroy('cart');
        $location.path('login');
    }
}])

// Dashboards
.controller('appCtrl', ['$scope', '$rootScope', '$state', '$location', '$ionicPopup', '$ionicLoading', 'Projects', 'AuthenticationService', 'sessionService', 'dataPrint',
                        '$mdDialog', '$mdSidenav', 'nmFormat', 'md5', 'receiptPrint', 'Scanner', 'deviceDetector',
                        function($scope, $rootScope, $state, $location, $ionicPopup, $ionicLoading, Projects, AuthenticationService, sessionService, dataPrint,
                                $mdDialog, $mdSidenav, nmFormat, md5, receiptPrint, Scanner, deviceDetector) {
    
    //console.log(deviceDetector);
    
    //$(document).scannerDetection({
    //    timeBeforeScanTest: 200, // wait for the next character for upto 200ms
    //    startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
    //    endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
    //    avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
    //    //ignoreIfFocusOn: 'input', // turn off scanner detection if an input has focus
    //    scanButtonKeyCode: 115,
    //    onReceive: function(data) {
    //        console.log(data);
    //    }, // receive callback
    //    onComplete: function(barcode, qty){
    //        console.log(barcode);
    //        console.log(qty);
    //    }, // main callback function
    //    onError: function(err){
    //        console.log(err);
    //    }, // error callback
    //    onKeyDetect: function(event){
    //        console.log(event.which);
    //        return false;
    //    } // detect keycode
    //});
    
    //e.which >= 48 && e.which <= 57 Number (48 - 57)
    //e.which >= 42 && e.which <= 57 Number with Symbol Calculator (42 - 47)
    //e.which >= 42 && e.which <= 122 Number with Symbol Calculator (42 - 47) & Word (97 - 122)
    
    var keybar = false;
    $scope.xbarcode = '';
    $scope.kbarcode = function(dt) {
        $scope.xbarcode = dt;
        keybar = true;
    }
    $scope.cbarcode = function() {
        $scope.xbarcode = '';
        $('#xbarcode').val('');
    }
    
    var chars = [];
    var pressed = false;
    var hidScanner = function(e) {
        var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
        if (e.which >= 42 && e.which <= 122) {
            chars.push(String.fromCharCode(charCode));
        }
        console.log(e.which + ":" + chars.join("|"));
        
        if (pressed == false) {
            var wait = $(this).data('wait');
            if (wait) {
                clearTimeout(wait);
            }
            
            wait = setTimeout(function() {
                        //if (chars.length >= 10) {
                            var barcode = chars.join("");
                            $rootScope.$broadcast("hidScanner::scanned", {barcode: barcode});
                        //}
                        chars = [];
                        pressed = false;
                    }, 200);
            
            $(this).data('wait', wait);
            return false;
        }
        pressed = true;
    }
    document.addEventListener('keypress', hidScanner, false);
    
    $rootScope.$on("hidScanner::scanned", function(event, args) {
        if (keybar == false) {
            $scope.xbarcode = (args.barcode != '') ? $scope.xbarcode+''+args.barcode : args.barcode;
            $('#xbarcode').val($scope.xbarcode);
        } else {
            keybar = false;
        }
        
        console.log($scope.xbarcode);
        $scope.addtocart({barcode: $scope.xbarcode, keypress: true});
        $rootScope.$apply();
    })
    
    $scope.path = $location.path();
    $scope.content = {url1: 'templates/v1/cashier/cashier.html'};
    $scope.sidebar = {url1: 'templates/v1/sidebar/menu.html',
                      url2: 'templates/v1/sidebar/cart.html',
                      url3: 'templates/v1/checkout/discharge.html',
                      url4: 'templates/v1/checkout/pmethod.html'};
    
    var dtAcc = JSON.parse(sessionService.get('acc_css'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.sr = {searchTerm: ''};
    
    // Slice Name
    $scope.sname = function(dt) {
        if (dt != undefined) {
            return dt.substring(0, 2);
        }
    }
    
    // Refresh Data
    $scope.refreshData = function() {
        //var outlet = JSON.parse(sessionService.get('outlet'));
        //angular.extend(dtAcc, outlet);
        //console.log(dtAcc);
        
        $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/refresh_data', dtAcc).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                angular.forEach(line.data.outlets, function(val) {
                    if (val.outlet_id == dtAcc.outlet_id) {
                        sessionService.set('favorite', JSON.stringify(val.favorite_products));
                        sessionService.set('categories', JSON.stringify(val.categories));
                        
                        dt1 = val.categories;
                        if ($scope.tabs != undefined && $scope.tabs == 1) {
                            $scope.htitle = 'Dashboard';
                            $scope.ttitle = 'Products';
                            $scope.type = 1;
                            $scope.pp = val.categories;
                        }
                        
                        dt2 = val.favorite_products;
                        if ($scope.tabs != undefined && $scope.tabs == 2) {
                            $scope.ttitle = 'Favorites';
                            $scope.type = 1;
                            $scope.pp = val.favorite_products; 
                        }
                        
                        delete val.favorite_products;
                        delete val.categories;
                        sessionService.set('outlet', JSON.stringify(val));
                        
                        $ionicLoading.hide();
                    }
                })
            }
        }, function() {
            $ionicLoading.hide();
        })
        
        $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/packages/index/1000', dtAcc).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                sessionService.set('packages', JSON.stringify(line.data));
                dt3 = line.data;
                if ($scope.tabs != undefined && $scope.tabs == 3) {
                    $scope.ttitle = 'Packages';
                    $scope.type = 1;
                    $scope.pp = line.data;
                }
            }
        }, function() {
            $ionicLoading.hide();
        })
    }
    
    $scope.editCart = function(dt) {
        $scope.ec = dt;
        $ionicPopup.show({
            template: '<input type="number" name="qty" ng-model="ec.qty" ng-value="ec.qty" class="pad-all-8 txl-center" placeholder="Qty" autofocus>'
                    +'<textarea name="desc" ng-model="ec.desc" class="mg-t-10 pad-all-8 ff-16" rows="5" placeholder="Description">ec.desc</textarea>',
            title: $scope.ec.product_name,
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel', type: 'btn btn-primary cl-mg-all min-hg ff-16',
                    onTap: function(e) {
                        var cart = JSON.parse(sessionService.get('cart'));
                        $scope.cart = cart;
                        $scope.subtotal = $scope.xsubtotal();
                    }
                },
                {
                    text: '<b>Update</b>', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                    onTap: function(e) {
                        console.log($scope.ec);
                        var cart = JSON.parse(sessionService.get('cart'));
                        if (Object.prototype.toString.call(cart) === '[object Array]') {
                            var log = [];
                            var i = 0;
                            
                            angular.forEach(cart, function(values) {
                                if (values.product_id == $scope.ec.product_id) {
                                    if ($scope.ec.qty > 0) {
                                        angular.extend(values, {qty: $scope.ec.qty, desc: $scope.ec.desc});
                                        log[i] = values;
                                        i++;
                                    }
                                } else {
                                    log[i] = values;
                                    i++;
                                }
                            });
                            
                            sessionService.set('cart', JSON.stringify(log));
                            
                            var cart = JSON.parse(sessionService.get('cart'));
                            $scope.cart = cart;
                            $scope.subtotal = $scope.xsubtotal();
                        }
                    }
                }
            ]
        });
    }
    
    if ($location.path() == '/dashboard') {
        var dt1 = JSON.parse(sessionService.get('categories'));
        var dt2 = JSON.parse(sessionService.get('favorite'));
        var dt3 = JSON.parse(sessionService.get('packages'));
        
        $scope.pic = Projects.zImg;
        $scope.cart = JSON.parse(sessionService.get('cart'));
        
        $scope.categories = function() {
            $scope.htitle = 'Dashboard';
            $scope.ttitle = 'Products';
            $scope.type = 1;
            $scope.pp = dt1;
            $scope.tabs = 1;
        }
        $scope.categories();
        
        $scope.allitems = function() {
            var log = [];
            var idx = '';
            var i = 0;
            
            angular.forEach(dt1, function(values) {
                angular.forEach(values.products, function(v1) {
                    var status = 0;
                    angular.forEach(log, function(v2) {
                        if (v2.product_id == v1.product_id) {
                            status = 1;
                        }
                    });
                    
                    if (status == 0) {
                        if (idx != v1.product_id) {
                            idx = v1.product_id;
                            log[i] = v1;
                            i++;
                        }
                    }
                })
            })
            
            angular.forEach(dt2, function(v1) {
                var status = 0;
                angular.forEach(log, function(v2) {
                    if (v2.product_id == v1.product_id) {
                        status = 1;
                    }
                });
                
                if (status == 0) {
                    if (idx != v1.product_id) {
                        idx = v1.product_id;
                        log[i] = v1;
                        i++;
                    }
                }
            })
            
            $scope.htitle = 'All Items';
            $scope.type = 2;
            $scope.pp = log;
            $scope.tabs = 1;
        }
        
        $scope.products = function(id) {
            var log = [];
            var ctitle;
            
            angular.forEach(dt1, function(values) {
                if(values.category_id == id)
                {
                    log.push = values.products;
                    ctitle = values.category_name
                }
            })
            
            $scope.htitle = ctitle;
            $scope.type = 2;
            $scope.pp = log.push;
            $scope.tabs = 1;
        }
        
        $scope.favorite = function() {
            $scope.ttitle = 'Favorites';
            $scope.type = 1;
            $scope.pp = dt2;
            $scope.tabs = 2;
        }
        
        $scope.packages = function() {
            $scope.ttitle = 'Packages';
            $scope.type = 1;
            $scope.pp = dt3;
            $scope.tabs = 3;
        }
        
        $scope.csprice = function() {
            $scope.ttitle = 'Custom Price';
            $scope.type = 1;
        }
        
        $scope.cprice = '';
        $scope.csnumber = function(dt) {
            if (dt == undefined) {
                $scope.cprice = dt;
            } else {
                var cprice = $scope.cprice; console.log(cprice);
                if (cprice == '' || cprice == undefined) {
                    cprice = '';
                } else {
                    cprice = parseFloat(cprice.replace(/[^\w\s]/gi, '')); // Remove All Symbol
                }
                
                if (cprice == '' && dt == 0) {
                    dt = '';
                }
                
                $scope.cprice = nmFormat(cprice+''+dt);
            }
           
        }
        $scope.csadd = function() {
            var cart = JSON.parse(sessionService.get('cart'));
            if (cart == '' || cart == undefined) {
                var lg = 0;
            } else {
                var lg = cart.length;
            }
            
            var cprice = $scope.cprice;
            cprice = parseFloat(cprice.replace(/[^\w\s]/gi, ''));
            
            var d = new Date();
            var pid = md5.createHash(dtAcc.user_id+'-'+d.getTime()+'-'+lg);
            
            if (Object.prototype.toString.call(cart) === '[object Array]') {
                var log = [];
                
                log[cart.length] = {product_id: 1, product_code: pid, product_name: 'Custom Price', product_sales_price: cprice, product_type: 'charge', qty: 1, desc: ''};
                angular.extend(cart, log);
                
                sessionService.set('cart', JSON.stringify(cart));
                $scope.cart = cart;
            } else {
                sessionService.set('cart', JSON.stringify([{product_id: 1, product_code: pid, product_name: 'Custom Price', product_sales_price: cprice, product_type: 'charge', qty: 1, desc: ''}]));
                $scope.cart = [{product_id: 1, product_code: pid, product_name: 'Custom Price', product_sales_price: cprice, qty: 1, desc: ''}];
            }
            
            $scope.subtotal = $scope.xsubtotal();
            $scope.cprice = '';
        }
        
        // Shopping Cart
        $scope.addtocart = function(id, pack) {
            var line;
            if (id.barcode != undefined) {
                var keyp = id.keypress;
                var idx = id.barcode;
                var dt = idx.split("*");
                
                if (dt.length > 1) {
                    id = dt[1];
                    if (dt[0] > 0) {
                        var xqty = dt[0];
                    } else {
                        var xqty = 1;
                    }                    
                } else {
                    id = id.barcode;
                    var xqty = 1;
                }
            } else {
                var keyp = false;
                var xqty = 1;
            }
            
            if (pack == true) {
                angular.forEach(dt3, function(v3) {
                    if (v3.package_id == id) {
                        line = v3.package_products;
                    }   
                })
            } else {
                angular.forEach(dt1, function(v1) {
                    angular.forEach(v1.products, function(v1) {
                        if (keyp == true) {
                            if (v1.product_code.toLowerCase() ==  id.toLowerCase()) {
                                line = v1
                            }
                        } else {
                            if (v1.product_id == id) {
                                line = v1
                            }
                        }
                    })
                });
                
                angular.forEach(dt2, function(v2) {
                    if (keyp == true) {
                        console.log(id.toLowerCase());
                        if (v2.product_code.toLowerCase() ==  id.toLowerCase()) {
                            line = v2
                            $scope.xbarcode = '';
                            $('#xbarcode').val('');
                        }
                    } else {
                        if (v2.product_id == id) {
                            line = v2
                        }
                    }
                });
            }
            
            var cart = JSON.parse(sessionService.get('cart'));
            
            if (Object.prototype.toString.call(cart) === '[object Array]') {
                var spack = [];
                var sp = 0;
                var slog = [];
                var i = 0;
                var status = 0;
                
                //console.log(line);
                if (line != undefined) {
                    angular.forEach(cart, function(values) {
                        if (pack == true) {
                            var stpack = 0;
                            angular.forEach(line, function(val) {
                                if (values.product_code == val.product[0].product_code) {
                                    spack[sp] = angular.extend(val.product[0], {qty: values.qty+parseFloat(xqty), status: 1});
                                    stpack = 1;
                                    sp++;
                                    
                                    slog[i] = val.product[0]; console.log(val.product[0]);
                                    status = 1;
                                    i++;
                                }
                            })
                            
                            if (stpack == 0) {
                                console.log(values);
                                slog[i] = values;
                                status = 1;
                                i++;
                            }
                        } else {
                            if (values.product_code == line.product_code) {
                                angular.extend(values, {qty: values.qty+parseFloat(xqty)});
                                slog[i] = values;
                                status = 1;
                            } else {
                                slog[i] = values;
                            }
                            i++;
                        }
                    })
                    //console.log(slog); console.log(status);
                    
                    if (pack == true) {
                        var stp = 0;
                        angular.forEach(line, function(v1) {
                            angular.forEach(spack, function(v2) {
                                if (v2.product_code == v1.product[0].product_code) {
                                    stp = 1;
                                }
                            })
                            
                            if (stp == 0) {
                                slog[i] = angular.extend(v1.product[0], {qty: parseFloat(xqty), desc: ''});
                                i++;
                            }
                            
                            stp = 0;
                        })
                    }
                    
                    if (status == 0) {
                        angular.extend(line, {qty: parseFloat(xqty), desc: ''});
                        slog[i] = line; 
                    }
                    
                    angular.extend(cart, slog);
                    sessionService.set('cart', JSON.stringify(cart));
                }
                
                $scope.cart = cart;
                $scope.subtotal = $scope.xsubtotal();
            } else {
                if (line != undefined) {
                    if (pack == true) {
                        var log = [];
                        var x = 0;
                        angular.forEach(line, function(val) {
                            log[x] = angular.extend(val.product[0], {qty: parseFloat(xqty), desc: ''});
                            x++;
                        })
                        sessionService.set('cart', JSON.stringify(log));
                        $scope.cart = log;
                    } else {
                        angular.extend(line, {qty: parseFloat(xqty), desc: ''});
                        sessionService.set('cart', JSON.stringify([line]));
                        $scope.cart = [line];
                    }
                    
                    $scope.subtotal = $scope.xsubtotal();
                }
            }
            
            //var cart = JSON.parse(sessionService.get('cart'));
            //if (cart == '' || cart == undefined) {
            //    var lg = 0;
            //} else {
            //    var lg = cart.length;
            //}
            //
            //var d = new Date();
            //var pid = md5.createHash(dtAcc.user_id+'-'+d.getTime()+'-'+lg);
            //var spack = [{package_id: 2, package_name: 'Discount Merdeka', package_price: 10000,
            //            package_products: [{product_id: 1, name: 'Test Data 1', qty: 2},
            //                               {product_id: 2, name: 'Test Data 2', qty: 3}]}];
            //var log = [];
            //var pack = [];
            //var p = 0;
            //var x = 0;
            //
            //angular.forEach(spack, function(v1) {
            //    angular.forEach(v1.package_products, function(v2) {
            //        angular.forEach(cart, function(v3) {
            //            if (v2.product_id == v3.product_id) {
            //                var s1 = v3.qty/v2.qty;
            //                log[x] = Math.floor(s1);
            //                x++;
            //            }
            //        })
            //    })
            //    
            //    cpack = v1.package_products;
            //    console.log(cpack.length+'/'+log.length);
            //    if (cpack.length == log.length) {
            //        pack[p] = angular.extend(v1, {package_accept: log});
            //        console.log(pack);
            //        p++;
            //    }
            //    log = [];
            //    x = 0;
            //}) 
        }
        
        // Back to Cart (Draft)
        $scope.backtocart = function(pp) {
            console.log(pp);
            var cart_draft = JSON.parse(sessionService.get('cart_draft'));
            
            if (Object.prototype.toString.call(cart_draft) === '[object Array]') {
                var log = [];
                var i = 0;
                
                angular.forEach(cart_draft, function(val) {
                    if (val.id != pp.id) {
                        log[i] = val;
                        i++;
                    }
                })
                
                sessionService.set('cart_draft', JSON.stringify(log));
                $scope.pp = JSON.parse(sessionService.get('cart_draft'));
            }
            
            sessionService.set('cart', JSON.stringify(pp.data));
            $scope.ss = {};
            $scope.cart = JSON.parse(sessionService.get('cart'));
            $scope.subtotal = $scope.xsubtotal();
            sessionService.set('salesno', pp.id);
        }
        
        $scope.clearsale = function() {
            var confirm = $mdDialog.confirm()
                            .title('Message')
                            .htmlContent('Do you want to clear sales?')
                            .ariaLabel('Alert')
                            .targetEvent('')
                            .ok('OK')
                            .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                $scope.ss = {};
                $scope.cart = sessionService.destroy('cart');
                $scope.subtotal = $scope.xsubtotal();
            });
        }
        
        // Outlet
        var outlet = JSON.parse(sessionService.get('outlet'));
        $scope.outlet = outlet.outlet_info;
        
        // Subtotal
        $scope.xsubtotal = function(type) {
            var cart = JSON.parse(sessionService.get('cart'));
            var val = 0;
            
            angular.forEach(cart, function(values) {
                if (values.product_type == 'discount') {
                    val = val - (values.product_sales_price*values.qty);
                } else {
                    val = val + (values.product_sales_price*values.qty);
                }
            })
            
            var subtotal = val;
            if (type == 1) {
                if ($scope.ss != undefined && $scope.ss.sales_receipt_disc_amount != undefined && $scope.ss.sales_receipt_disc_amount != 0) {
                    val = val - $scope.ss.sales_receipt_disc_amount;
                }
                
                if ($scope.outlet.outlet_tax != undefined && $scope.outlet.outlet_tax != 0) {
                    tax = ($scope.outlet.outlet_tax/100) * subtotal;
                    val = val + tax;
                }
                
                if ($scope.ss != undefined && $scope.ss.sales_receipt_charges_amount != undefined && $scope.ss.sales_receipt_charges_amount != 0) {
                    val = val + $scope.ss.sales_receipt_charges_amount;
                }
            }
            
            if (type > 0) {
                return val;
            } else {
                return nmFormat(val);
            }
        }
        $scope.subtotal = $scope.xsubtotal(0);
        
        $scope.zsubtotal = function() {
            var cart = JSON.parse(sessionService.get('cart'));
            var subtotal = 0;
            
            angular.forEach(cart, function(values) {
                if (values.product_type == 'discount') {
                    subtotal = subtotal - (values.product_sales_price*values.qty);
                } else {
                    subtotal = subtotal + (values.product_sales_price*values.qty);
                }
            })
            
            return subtotal;
        }
        
        // Tax
        $scope.tax = function(type) {
            var subtotal = $scope.zsubtotal();
            
            var val = '';
            if ($scope.outlet.outlet_tax != undefined && $scope.outlet.outlet_tax != 0) {
                val = ($scope.outlet.outlet_tax/100) * subtotal;
                angular.extend($scope.ss, {sales_receipt_tax: $scope.outlet.outlet_tax, sales_receipt_tax_amount: val});
            }
            
            if (type > 0) {
                return val;
            } else {
                return nmFormat(val);
            }
        }
        
        // Total
        $scope.total = function(type) {
            return nmFormat($scope.xsubtotal(type));
        }
        
        $scope.valFormat = function(val) {
            return nmFormat(val);
        }
        
        // Finish Transaction
        $scope.cpay = {sales_receipt_paymethod: 0, sales_receipt_status: 'done'};
        $scope.payFinish = function(cpay) {
            var status = 1;
            var order = {};
            var cart = JSON.parse(sessionService.get('cart'));
            var d = new Date(); console.log(cpay);
            var xmonth = parseFloat(d.getMonth())+1;
            var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
            var xtime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
            
            // Empty Cart
            if ($scope.xsubtotal(1) == 0) {
                status = 0;
                $mdSidenav('cart').close();
                $mdSidenav('pmethod').close();
                $mdDialog.show(
                    $mdDialog.alert()
                      .parent(angular.element(document.body))
                      .clickOutsideToClose(true)
                      .title('Message')
                      .textContent('The Cart is empty')
                      .ariaLabel('Transaction')
                      .ok('OK')
                      .targetEvent(null)
                );
                
                return false;
            }
            
            // Empty Paid
            if (cpay.sales_receipt_paymethod == 'cash') {
                if (cpay.sales_receipt_paid == undefined) {
                    status = 0;
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .clickOutsideToClose(true)
                          .title('Message')
                          .textContent('The Cash is empty')
                          .ariaLabel('Transaction')
                          .ok('OK')
                          .targetEvent(null)
                    );
                    
                    return false;
                }
            }
            
            if (cpay.sales_receipt_paymethod == undefined) {
                status = 0;
                $mdDialog.show(
                    $mdDialog.alert()
                      .parent(angular.element(document.body))
                      .clickOutsideToClose(true)
                      .title('Message')
                      .textContent('Choose payment method first')
                      .ariaLabel('Transaction')
                      .ok('OK')
                      .targetEvent(null)
                );
                
                return false;
            }
            
            // Payment Method & Check Cash Paid
            if (cpay.sales_receipt_paymethod != 'cash') {
                angular.extend(cpay, {sales_receipt_paid: $scope.xsubtotal(1)})
            } else {
                if (cpay.sales_receipt_paid < $scope.xsubtotal(1)) {
                    status = 0;
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .clickOutsideToClose(true)
                          .title('Message')
                          .textContent('Your payment is less')
                          .ariaLabel('Transaction')
                          .ok('OK')
                          .targetEvent(null)
                    );
                    
                    return false;
                }
            }
            
            // Finish
            if (cpay.sales_receipt_paymethod != undefined && status > 0) {
                //var inet = JSON.parse(sessionService.get('enableinet'));
                var sno = sessionService.get('salesno');
                
                if (cart.sales_data != undefined && cart.sales_data.sales_receipt_no != '') {
                    var sales_no = cart.sales_data.sales_receipt_no;
                } else {
                    var sales_no = md5.createHash(dtAcc.user_id+'-'+d.getTime());
                }
                
                if (sno == undefined) {
                    sales_no = sales_no.substring(0, 6);
                } else {
                    sales_no = sno;
                }
                
                if ($scope.ss.sales_receipt_charges_amount == undefined || $scope.ss.sales_receipt_charges_amount == '') {
                    angular.extend(cpay, {sales_receipt_charges: 0, sales_receipt_charges_amount: 0});
                }
                
                if ($scope.ss.sales_receipt_disc_amount == undefined || $scope.ss.sales_receipt_disc_amount == '') {
                    angular.extend(cpay, {sales_receipt_disc: 0, sales_receipt_disc_amount: 0});
                }
                
                angular.extend(cpay, {sales_date: d.getTime(), sales_offline_no: sales_no, sales_receipt_date: xdate+' '+xtime,
                                      sales_receipt_no: sales_no, sales_receipt_items: cart, sales_receipt_subtotal: $scope.zsubtotal(), sales_receipt_total: $scope.xsubtotal(1)});
                angular.extend(cpay, $scope.ss);
                angular.extend(order, {sales_data: cpay});
                angular.extend(order, dtAcc);
                
                var status = 0;
                sessionService.set('print', JSON.stringify(cpay));
                
                $scope.paySuccess = function(status) {
                    // Temp Success Order
                    var tmporder = {};
                    angular.extend(tmporder, {api: status, online: status, user_data: dtAcc});
                    angular.extend(tmporder, cpay); console.log(tmporder);
                    $scope.tempOrders(tmporder);
                    
                    $mdDialog.show(
                        $mdDialog.alert()
                          .parent(angular.element(document.body))
                          .clickOutsideToClose(true)
                          .title('Message')
                          .htmlContent('<p>Transaction Success</p><h3>CHANGE</h3><h1>Rp '+$scope.payChange+'</h1>')
                          .ariaLabel('Transaction')
                          .ok('OK')
                          .targetEvent(null)
                    ).then(function() {
                        $scope.xbarcode = '';
                        $('#xbarcode').val('');
                        
                        $scope.ss = {};
                        $scope.cart = sessionService.destroy('cart');
                        $scope.subtotal = $scope.xsubtotal();
                        
                        $scope.pMethod(0);
                        $mdSidenav('cart').close();
                        $mdSidenav('discharge').close();
                        $mdSidenav('pmethod').close();
                        
                        $('.pay-cash').hide();
                        $('.pay-form').hide();
                        $('.pay-card').hide();
                        
                        sessionService.destroy('salesno');
                        //$state.go($state.current, {}, {reload: true});
                    });
                    
                    $scope.printData();
                }
                
                //if (inet == true) {
                //    $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
                //    Projects.curlPost(Projects.zUrl, 'api/sales/create', order).then(function(msg) {
                //        var line = msg.data;
                //        if(msg.status == 200)
                //        {
                //            status = 1;
                //            $scope.paySuccess(status);
                //        }
                //    }, function(err) {
                //        status = 0;
                //        $scope.paySuccess(status);
                //    })
                //} else {
                    status = 0;
                    $scope.paySuccess(status);
                //}
            }
        }
        
        // Discharge (Discount / Charges)
        $scope.ss = {};
        $scope.salesDischarge = function(ss, val, type) {
            var subtotal = $scope.zsubtotal();
            
            if (subtotal != '') {
                if (type > 0) {
                    var vv = (ss[val] != undefined && ss[val] != '') ? Math.round(ss[val]) : '';
                    var v = (ss[val] != undefined && ss[val] != '') ? ((ss[val]/subtotal) * 100) : '';
                    v = (v == '') ? '' : v.toFixed(2);
                    
                    switch(val)
                    {
                        case 'sales_receipt_disc_amount':
                            angular.extend($scope.ss, {sales_receipt_disc: v, sales_receipt_disc_amount: vv});
                        break;
                        case 'sales_receipt_charges_amount':
                            angular.extend($scope.ss, {sales_receipt_charges: v, sales_receipt_charges_amount: vv});
                        break;
                    }
                } else {
                    var v = (ss[val] != undefined && ss[val] != '') ? Math.round(((ss[val]/100) * subtotal)) : '';
                    var vv = (ss[val] != undefined && ss[val] != '') ? ss[val] : '';
                    
                    switch(val)
                    {
                        case 'sales_receipt_disc':
                            angular.extend($scope.ss, {sales_receipt_disc: vv, sales_receipt_disc_amount: v});
                        break;
                        case 'sales_receipt_charges':
                            angular.extend($scope.ss, {sales_receipt_charges: vv, sales_receipt_charges_amount: v});
                        break;
                    }
                }
                
                $scope.total(1);
            } else {
                $scope.ss = {};
            }
        }
        
        // Payment Method
        $scope.pMethod = function(val) {
            $scope.cpay = {sales_receipt_paymethod: val.pm, sales_receipt_status: 'done', sales_receipt_paid: val.paid,
                           sales_receipt_cardno: '', sales_receipt_apprno: ''};
            $scope.srp = {paid: ''};
            $scope.payChange = 0;
        }
        
        $scope.srp = {paid: ''};
        $scope.salesPaid = function(srp, val) {
            var cash = srp.paid;
            if (cash != undefined) {
                cash = parseFloat(cash.replace(/[^\w\s]/gi, '')); // Remove All Symbol
            }
            
            $scope.pMethod({pm: val.sales_receipt_paymethod, paid: cash});
            $scope.srp = {paid: nmFormat(cash)};
            $scope.payChange = nmFormat(parseFloat(cash) - $scope.xsubtotal(1));
        }
        $scope.payChange = 0;
        
        // Temp Orders
        $scope.tempOrders = function(temp) {
            var temps = JSON.parse(sessionService.get('orders'));
            
            if (Object.prototype.toString.call(temps) === '[object Array]') {
                var log = [];
                var i = 0;
                var status = 0;
                
                angular.forEach(temps, function(val) {
                    if (val.sales_offline_no == temp.sales_offline_no) {
                        log[i] = temp;
                        status = 1;
                    }
                    
                    i++;
                })
                
                if (status == 0) {
                    log[i] = temp;
                    angular.extend(temps, log);
                }
                
                sessionService.set('orders', JSON.stringify(temps));
            } else {
                sessionService.set('orders', JSON.stringify([temp]));
            }
        }
    }
    
    $scope.orderNew = function() {
        var cart = JSON.parse(sessionService.get('cart'));
        
        if (Object.prototype.toString.call(cart) === '[object Array]') {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Message',
                template: 'Do you want to clear sales?',
                buttons: [
                    { text: 'No', type: 'btn btn-primary cl-mg-all min-hg ff-16' },
                    {
                        text: '<b>Yes</b>', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                        onTap : function() {
                            $scope.ss = {};
                            $scope.cart = sessionService.destroy('cart');
                            $scope.subtotal = $scope.xsubtotal();
                        }
                    }
                ]
            });
        } else {
            $scope.ss = {};
            $scope.cart = sessionService.destroy('cart');
            $scope.subtotal = $scope.xsubtotal();
        }
    }
    
    $scope.orderSave = function() {
        $scope.dtr = {};
        var cart = JSON.parse(sessionService.get('cart'));
        
        if (Object.prototype.toString.call(cart) === '[object Array]') {
            // An elaborate, custom popup
            var popSave = $ionicPopup.show({
                template: '<input name="draft" ng-model="dtr.draft" class="pad-all-8 txl-center ff-18" autofocus>',
                title: 'Enter Draft Name',
                scope: $scope,
                buttons: [
                    { text: 'Cancel', type: 'btn btn-primary cl-mg-all min-hg ff-16' },
                    {
                        text: '<b>Save</b>', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                        onTap: function(e) {
                            if (!$scope.dtr.draft) {
                                e.preventDefault();
                            } else {
                                var d = new Date();
                                var sno = sessionService.get('salesno');
                                
                                if (sno == undefined) {
                                    var idx = md5.createHash(dtAcc.user_id+'-'+d.getTime())
                                    sales_no = idx.substring(0, 6);
                                } else {
                                    sales_no = sno;
                                }
                                
                                var cart = JSON.parse(sessionService.get('cart'));
                                var cart_draft = JSON.parse(sessionService.get('cart_draft'));
                                var dt = {id : sales_no, name : $scope.dtr.draft, date: d.getTime(), data : cart};
                                
                                if (Object.prototype.toString.call(cart_draft) === '[object Array]') {
                                    var i = cart_draft.length;
                                    cart_draft[i] = dt;
                                    sessionService.set('cart_draft', JSON.stringify(cart_draft));
                                } else {
                                    sessionService.set('cart_draft', JSON.stringify([dt]));
                                }
                                
                                $scope.ss = {};
                                $scope.cart = sessionService.destroy('cart');
                                $scope.subtotal = $scope.xsubtotal();
                                sessionService.destroy('salesno');
                                
                                if ($scope.type == 3) {
                                    $scope.pp = JSON.parse(sessionService.get('cart_draft'));
                                }
                            }
                        }
                    }
                ]
            });
        }
    }
    
    $scope.orderDraft = function() {
        $scope.htitle = 'Draft Cart';
        $scope.ttitle = 'Draft';
        $scope.type = 3;
        $scope.pp = JSON.parse(sessionService.get('cart_draft'));
        $mdSidenav('cart').close();
    }
    
    $scope.printTrial = function() {
        var cart = JSON.parse(sessionService.get('cart'));
        var d = new Date();
        var xmonth = parseFloat(d.getMonth())+1;
        var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
        var xtime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        
        var sales_no = md5.createHash(dtAcc.user_id+'-'+d.getTime());
        sales_no = sales_no.substring(0, 6);
        sessionService.set('salesno', sales_no);
        
        var pr = {sales_date: d.getTime(), sales_offline_no: sales_no, sales_receipt_date: xdate+' '+xtime,
                  sales_receipt_no: sales_no, sales_receipt_items: cart, sales_receipt_subtotal: $scope.zsubtotal(), sales_receipt_total: $scope.xsubtotal(1)};
        
        if ($scope.ss.sales_receipt_charges_amount == undefined || $scope.ss.sales_receipt_charges_amount == '') {
            angular.extend(pr, {sales_receipt_charges: 0, sales_receipt_charges_amount: 0});
        }
        
        if ($scope.ss.sales_receipt_disc_amount == undefined || $scope.ss.sales_receipt_disc_amount == '') {
            angular.extend(pr, {sales_receipt_disc: 0, sales_receipt_disc_amount: 0});
        }
        
        angular.extend(pr, $scope.ss);
        
        var dt = {user: dtAcc, order: pr};
        var doc = receiptPrint(dt, 1); console.log(doc);
        dataPrint(doc, true);  
    }
    
    $scope.printData = function() {
        var orders = JSON.parse(sessionService.get('print'));
        var dt = {user: dtAcc, order: orders};
        var doc = receiptPrint(dt); console.log(doc);
        dataPrint(doc, true);
    }
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    $scope.closeSide = function(val) {
        $mdSidenav(val).close()
    }
    
}])

.controller('activityCtrl', ['$scope', '$state', '$stateParams', '$location', '$ionicPopup', 'Projects', 'AuthenticationService', 'sessionService', 'dataPrint',
                             '$filter', '$mdDialog', '$mdSidenav', '$ionicLoading', 'nmFormat', 'md5', 'receiptPrint', 'summaryData', 'summaryPrint', 'deviceDetector',
                             function($scope, $state, $stateParams, $location, $ionicPopup, Projects, AuthenticationService, sessionService, dataPrint,
                             $filter, $mdDialog, $mdSidenav, $ionicLoading, nmFormat, md5, receiptPrint, summaryData, summaryPrint, deviceDetector) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.sidebar = {url1: 'templates/v1/sidebar/menu.html'};

    var dtAcc = JSON.parse(sessionService.get('acc_css'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    var xorder = JSON.parse(sessionService.get('orders'));
    
    // Slice Name
    $scope.sname = function(dt) {
        return dt.substring(0, 2);
    }
    
    // Date Time
    $scope.dtime = function(dt, type) {
        var d = (dt != '') ? new Date(dt) : new Date();
        
        if (type == undefined) {
            var xmonth = parseFloat(d.getMonth())+1;
            var xdate = d.getDate()+'-'+xmonth+'-'+d.getFullYear();
        } else {
            var month = ["January","Feburary","March","April","May","June","July","August","September","October","November","December"];
            var xdate = d.getDate()+' '+month[d.getMonth()]+' '+d.getFullYear();
        }
        
        var xtime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
        
        if (type == 2) {
            return xdate;
        } else {
            return xdate+' '+xtime;
        }
    }
    
    // Payment Method
    $scope.pmethod = function(pm) {
        var method = '';
        switch(pm) {
            case 'cash':
                method = 'Cash';
            break;
            case 'dc':
                method = 'Debet';
            break;
            case 'cc':
                method = 'Credit Card';
            break;
        }
        
        return method;
    }
    
    // Subtotal
    $scope.subtotal = function(dt, type) {
        var val = 0;
        
        angular.forEach(dt.sales_receipt_items, function(values) {
            if (values.product_type == 'discount') {
                val = val - (values.product_sales_price*values.qty);
            } else {
                val = val + (values.product_sales_price*values.qty);
            }
        })
        
        if (type == 2) {
            if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
                val = val - parseFloat(dt.sales_receipt_disc_amount);
            }
            
            if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
                val = val + parseFloat(dt.sales_receipt_tax_amount);
            }
        }
        
        if (type == 3) {
            if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
                val = val - parseFloat(dt.sales_receipt_disc_amount);
            }
            
            if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
                val = val + parseFloat(dt.sales_receipt_tax_amount);
            }
            
            if (dt.sales_receipt_charges_amount != undefined && dt.sales_receipt_charges_amount != 0) {
                val = val + parseFloat(dt.sales_receipt_charges_amount);
            }
        }
        
        if (type > 0) {
            return val;
        } else {
            return nmFormat(val);
        }
    }
    
    // Total
    $scope.total = function(dt, type) {
        return nmFormat($scope.subtotal(dt, type));
    }
    
    // Change
    $scope.change = function(dt) {
        var change = nmFormat(parseFloat(dt.sales_receipt_paid) - $scope.subtotal(dt, 2));
        
        if (change == '') {
            return 0;
        } else {
            return change;
        }
    }
    
    $scope.valFormat = function(val) {
        return nmFormat(val);
    }
    
    $scope.dd = '';
    $scope.actReceipt = function(dt) {
        $scope.dd = dt;
    }
    
    var ndata = $filter('orderBy')(xorder, 'sales_date', true);
    $scope.dataSummary = function(dt) {
        var zdata = (dt == undefined) ? ndata : dt;
        var xdata = [];
        var summ = '';
        var x1 = 0;
        var x2 = 0;
        angular.forEach(zdata, function(val) {
            var dtime = (val.sales_date == undefined) ? val.sales_receipt_date : val.sales_date;
            
            var d = new Date(dtime);
            var month = ["January","Feburary","March","April","May","June","July","August","September","October","November","December"];
            var xdate = d.getDate()+' '+month[d.getMonth()]+' '+d.getFullYear();
            
            var xmonth = parseFloat(d.getMonth())+1;
            var ydate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
            
            if (x1 == 0) {
                summ = $scope.subtotal(val, 3);
                xdata[x1] = {created_at: xdate, sales_date: ydate, sales_summary: summ, sales_data: [val]};
                x1++;
            } else {
                if (xdata[x2].created_at == xdate) {
                    summ = $scope.subtotal(val, 3);
                    angular.extend(xdata[x2], {sales_summary: xdata[x2].sales_summary + summ})
                    xdata[x2].sales_data.push(val);
                } else {
                    summ = $scope.subtotal(val, 3);
                    xdata[x1] = {created_at: xdate, sales_date: ydate, sales_summary: summ, sales_data: [val]};
                    x1++; x2++;
                }            
            }
        })
        
        return xdata;
    }
    
    // Sales Summary
    var d = new Date();
    var month = ["January","Feburary","March","April","May","June","July","August","September","October","November","December"];
    var xmonth = parseFloat(d.getMonth())+1;
    var nowdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
    $scope.ec = {trans_date: nowdate};
    
    $scope.reSummary = function(ec) {
        if (ec.date != undefined) {
            var d = new Date(ec.date);
            var xnmonth = parseFloat(d.getMonth())+1;
            var newdate = d.getFullYear()+'-'+xnmonth+'-'+d.getDate();
            angular.extend(ec, {trans_date: newdate, start_date: newdate, end_date: newdate});
            angular.extend(dtAcc, ec);
            $scope.salesDaily();
        } else {
            angular.extend(dtAcc, {start_date: ec.trans_date, end_date: ec.trans_date});
            $scope.salesDaily();
        }
    }
    
    angular.extend(dtAcc, $scope.ec);
    $scope.salesDaily = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
        
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/sales/index', dtAcc).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $ionicLoading.hide();
                sessionService.set('actOnline', true);
                sessionService.set('activity', JSON.stringify(line.data));
                $scope.pp = $scope.dataSummary(line.data);
                $scope.ss = summaryData($scope.pp);
                dt1 = $scope.pp;
            }
        }, function() {
            $ionicLoading.hide();
        })
    }
    
    $scope.resetSummary = function() {
        sessionService.set('actOnline', false);
        $scope.pp = $scope.dataSummary();
    }
    
    var actOL = sessionService.get('actOnline');console.log(actOL);
    var actData = JSON.parse(sessionService.get('activity'));
    if (actOL == 'false') {
        dt1 = $scope.dataSummary();
    } else {
        dt1 = $scope.dataSummary(actData);
    }
    
    $scope.salesSummary = function() {
        $scope.ss = summaryData(dt1);
    }
    
    $scope.htitle = 'Back';
    
    if ($location.path() == '/activity') {
        $scope.content = {url1: 'templates/v1/activity/activity.html', url2: 'templates/v1/activity/summary.html'};
        
        $scope.pp = '';     
        $scope.activity = function() {
            $scope.ttitle = 'Activity';
            $scope.type = 1;
            if (actOL == 'false') {
                $scope.pp = $scope.dataSummary();
            } else {
                $scope.pp = $scope.dataSummary(actData);
            }
        }
        $scope.activity();
        
        $scope.salesSummary();
    }
    
    if ($location.path() == '/summary') {
        $scope.content = {url1: 'templates/v1/activity/summary.html', url2: 'templates/v1/activity/summary.html'};
        $scope.ttitle = 'Summary';
        $scope.type = 2;
        
        $scope.salesSummary();
    }
    
    if ($location.path() == '/receipt/'+$stateParams.id) {
        $scope.content = {url1: 'templates/v1/activity/receipt.html', url2: 'templates/v1/activity/receipt.html'};
        $scope.ttitle = 'Receipt No '+$stateParams.id;
        $scope.type = 2;
        
        var log = '';
        angular.forEach(dt1, function(v1) {
            angular.forEach(v1.sales_data, function(v2) {
                if (v2.sales_receipt_no == $stateParams.id) {
                    log = v2;
                }
            })
        })
        $scope.dd = log;
    }
    
    $scope.printSummary= function(ec) {
        console.log(ec);
        console.log(dt1);
        if (ec.date != undefined) {
            var d = new Date(ec.date);
            var xmonth = parseFloat(d.getMonth())+1;
            var newdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
        } else {
            var newdate = nowdate;
        }
        
        var ss = $filter('filter')(dt1, {sales_date: newdate});
        console.log(ss);
        if (ss != '') {
            var doc = summaryPrint(ss); console.log(doc);
            dataPrint(doc);
        }
    }
    
    $scope.sendReceipt = function(dt) {
        sessionService.set('print', JSON.stringify(dt));
        
        $scope.ec = {};
        $ionicPopup.show({
            template: '<h3 class="txl-center">Rp '+$scope.total(dt, 3)+'</h3><p class="txl-center">Would you like an email receipt?</p>'
                      +'<input ng-model="ec.email" class="pad-all-8 txl-center" placeholder="Email Receipt" autofocus>',
            title: "Receipt No "+dt.sales_receipt_no,
            scope: $scope,
            buttons: [
                {
                    text: 'No, Thanks', type: 'btn btn-primary cl-mg-all min-hg ff-16',
                    onTap: function(e) {}
                },
                {
                    text: 'Print Receipt', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                    onTap: function(e) {
                        if ($scope.ec.email != undefined && $scope.ec.email != '') {
                            $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
                            
                            angular.extend(dtAcc, {customer_data: {customer_email: $scope.ec.email}});
                            console.log(dtAcc);
                            
                            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                            Projects.curlPost(Projects.zUrl, 'api/customers/create', dtAcc).then(function(msg) {
                                var line = msg.data;
                                if(msg.status == 200)
                                {
                                    $ionicLoading.hide();
                                }
                            }, function() {
                                $ionicLoading.hide();
                            })
                        }
                        
                        var orders = JSON.parse(sessionService.get('print'));
                        var dt = {user: orders.user_data, order: orders};  delete orders.user_data;
                        var doc = receiptPrint(dt); console.log(doc);
                        dataPrint(doc);
                    }
                }
            ]
        });
    }
    
    $scope.issueRef = function(dt) {
        $scope.ec = {reason: ''};
        $ionicPopup.show({
            template: '<h3 class="txl-center">Rp '+$scope.total(dt, 3)+'</h3><p class="ff-16 txl-center">Reason for refund</p>'
                        +'<ion-list>'
                            +"<ion-radio class='btn-grey cl-pad-all cl-mg-all' ng-model='ec.reason' ng-value='\"Returned Goods\"'>Returned Goods</ion-radio>"
                            +"<ion-radio class='btn-grey cl-pad-all cl-mg-all' ng-model='ec.reason' ng-value='\"Accident Charge\"'>Accident Charge</ion-radio>"
                            +"<ion-radio class='btn-grey cl-pad-all cl-mg-all' ng-model='ec.reason' ng-value='\"Canceled Order\"'>Canceled Order</ion-radio>"
                            +'<br><input ng-model="ec.reason" class="pad-all-8 ff-16 txl-center" placeholder="Other" autofocus>'
                        +'</ion-list>',
            title: "Issue Refund",
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel', type: 'btn btn-primary cl-mg-all min-hg ff-16',
                    onTap: function(e) {}
                },
                {
                    text: 'Refund', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                    onTap: function(e) {
                        console.log($scope.ec);
                        $ionicLoading.show({template: '<ion-spinner icon="lines"></ion-spinner>'});
                        
                        $scope.voider = function() {
                            if (actOL == 'false') {
                                var xdata = JSON.parse(sessionService.get('orders'));
                            } else {
                                var xdata = JSON.parse(sessionService.get('activity'));
                            }
                            
                            var log = [];
                            var x = 0;
                            angular.forEach(xdata, function(val) {
                                if (val.sales_receipt_no == dt.sales_receipt_no) {
                                    log[x] = angular.extend(val, {sales_receipt_status: 'void'});
                                    $scope.dd = val;
                                } else {
                                    log[x] = val;
                                }
                                
                                x++;
                            })
                            dt1 = log;
                            if (actOL == 'false') {
                                sessionService.set('orders', JSON.stringify(log));
                            }
                        }
                        $scope.vomsg = function(ev) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .parent(angular.element(document.body))
                                    .clickOutsideToClose(true)
                                    .title('Message')
                                    .textContent('Refund Order Successfully')
                                    .ariaLabel('Message')
                                    .ok('OK')
                                    .targetEvent(ev)
                            );
                        }
                        
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/sales/refund/'+dt.sales_receipt_no, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                $ionicLoading.hide();
                                $scope.voider();
                                $scope.vomsg(null);
                            }
                        }, function() {
                            $mdDialog.hide();
                            $ionicLoading.hide();
                            $scope.voider();
                            $scope.vomsg(null);
                        })
                    }
                }
            ]
        });
    }
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    $scope.closeSide = function(val) {
        $mdSidenav(val).close()
    }
    
}])

.controller('deviceCtrl', ['$scope', '$state', '$location', 'Projects', 'AuthenticationService', 'sessionService', 'dataPrint',
                           '$cordovaBluetoothLE', '$ionicPopup', '$ionicLoading', '$mdDialog', '$mdSidenav', 'nmFormat', 'md5', 'deviceDetector',
                           function($scope, $state, $location, Projects, AuthenticationService, sessionService, dataPrint,
                           $cordovaBluetoothLE, $ionicPopup, $ionicLoading, $mdDialog, $mdSidenav, nmFormat, md5, deviceDetector) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.sidebar = {url1: 'templates/v1/sidebar/menu.html'};

    var dtAcc = JSON.parse(sessionService.get('acc_css'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc; console.log(dtAcc);
    
    $scope.setContent = function(val) {
        switch (val) {
            case 'tools':
                $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/tools.html'};
            break;
            case 'taxes-gratuity':
                $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/taxes-gratuity.html'};
            break;
            case 'bluetooth':
                $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/bluetooth.html'};
                $scope.bluetooth();
            break;
            case 'printer':
                $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/printer.html'};
                //$scope.printer({port: 'Bluetooth'});
            break;
            case 'support':
                $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/support.html'};
            break;
        }
    }
    
    // Tools
    $scope.upTools = function(set, ev) {
        if (set.passcash != undefined && set.passcash != '') {
            $('#passcash').val('');
            angular.extend(dtAcc, {passcash: set.passcash});
            sessionService.set('acc_css', JSON.stringify(dtAcc));
            
            $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Message')
                .textContent('Set Password Successfully')
                .ariaLabel('Message')
                .ok('OK')
                .targetEvent(ev)
            );
        }
    }
    
    // Open Cash Drawer
    $scope.openCash = function() {
        $scope.ec = {};
        $ionicPopup.show({
            template: '<input ng-model="ec.pass" class="pad-all-8 txl-center" placeholder="Password" autofocus>',
            title: "Open Cash Drawer",
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel', type: 'btn btn-primary cl-mg-all min-hg ff-16',
                    onTap: function(e) {}
                },
                {
                    text: 'Open', type: 'btn btn-blue cl-mg-all min-hg ff-16',
                    onTap: function(e) {
                        if ($scope.ec.pass == dtAcc.passcash) {
                            var xprint = JSON.parse(sessionService.get('printSel'));
                            var blueth = sessionService.get('enableblue');
                            
                            var dvdetect = deviceDetector;
                            if (dvdetect.os == 'android' || dvdetect.os == 'ios' || dvdetect.os == 'windows-phone') {
                                if (blueth == 'true') {
                                    if (Object.prototype.toString.call(xprint) === '[object Object]') {
                                        var xpname = xprint.name.split(':');
                                        if (xpname[1] != '') {
                                            window.plugins.starPrinter.openCashDrawer(xprint.name, function(error, result){
                                                if (error) {
                                                    console.error(error);
                                                    $mdDialog.show(
                                                        $mdDialog.alert()
                                                        .parent(angular.element(document.body))
                                                        .clickOutsideToClose(true)
                                                        .title('Failed')
                                                        .htmlContent('<p>Failed to connect printer</p>')
                                                        .ariaLabel('Print')
                                                        .ok('OK')
                                                        .targetEvent(null)
                                                    );
                                                } else {
                                                    console.log("Open Cash Drawer Successfully");
                                                }
                                            });
                                        } else {
                                            dataPrint();
                                        }
                                    }
                                } else {
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                        .parent(angular.element(document.body))
                                        .clickOutsideToClose(true)
                                        .title('Failed')
                                        .htmlContent('<p>Please turn on your bluetooth</p>')
                                        .ariaLabel('Print')
                                        .ok('OK')
                                        .targetEvent(null)
                                    );
                                }
                            } else {
                                dataPrint();
                            }
                        } else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent('Wrong Password!')
                                .ariaLabel('Message')
                                .ok('OK')
                                .targetEvent(null)
                            );
                        }
                    }
                }
            ]
        });
    }
    
    // Bluetooth
    $scope.blueMsg = 'Searching Bluetooth... <i class="fa fa-spinner fa-pulse fa-fw"></i>';
    $scope.blueResult = [/*{name: 'Bluetooth', macAddress: '1234567890', status: 'Online'}*/];
    $scope.bluetooth = function() {
        setTimeout(function(){
            $cordovaBluetoothLE.initialize({request:true}).then(null, function(err) {
                console.log(err);
            }, function(obj) {
                console.log(obj);
                
                var ss = {
                            "services": [
                              "180D",
                              "180F"
                            ],
                            "allowDuplicates": true,
                            "scanMode": bluetoothle.SCAN_MODE_LOW_POWER,
                            "matchMode": bluetoothle.MATCH_MODE_STICKY,
                            "matchNum": bluetoothle.MATCH_NUM_MAX_ADVERTISEMENT,
                            //"callbackType": bluetoothle.CALLBACK_TYPE_ALL_MATCHES,
                          };
                $cordovaBluetoothLE.startScan(ss).then(function(err) {
                    console.log(err);
                }, function(obj) {
                    console.log(obj);
                }, function(obj) {
                    console.log(obj);
                    addDevice(obj);
                });
            });
        }, 200);
    }
    $scope.onBluetooth = function() {
        var blue = JSON.parse(sessionService.get('enableblue'));
        if (blue != undefined && blue == false) {
            $cordovaBluetoothLE.initialize({request:true}).then(null, function(err) {
                sessionService.set('enableblue', false);
            }, function(obj) {
                sessionService.set('enableblue', true);
            })
        }
    }
    
    function addDevice(obj) {
        if (obj.status == "scanStarted") {
            return;
        }
      
        if ($scope.blueResult[obj.address] !== undefined) {
            return;
        }
      
        obj.services = {};
        $scope.blueResult[obj.address] = obj;
        
        console.log($scope.blueResult);
    }
    
    // Printer
    $scope.mport = {port: 'Bluetooth'};
    $scope.cport = {port: 'Bluetooth'};
    $scope.printResult = [/*{name: 'Printer', macAddress: '1234567890', status: 'Online'}*/];
    $scope.printer = function(pr) {
        var blueth = sessionService.get('enableblue');
        if (blueth == 'false') {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title('Failed')
                .htmlContent('<p>Please turn on your bluetooth</p>')
                .ariaLabel('Print')
                .ok('OK')
                .targetEvent(null)
            );
            
            return false;
        }
        
        console.log(pr);
        $scope.printMsg = 'Searching Devices... <i class="fa fa-spinner fa-pulse fa-fw"></i>';
        setTimeout(function(){
            var log = [];
            var i = 0;
            window.plugins.starPrinter.portDiscovery(pr.port, function(error, printerList){
                if (error) {
                    $scope.printMsg = error;
                    console.log(error);
                } else {
                    if (printerList.length > 0) {
                        var xprint = JSON.parse(sessionService.get('printSel'));
                        angular.forEach(printerList, function(val) {
                            var st;
                            
                            if (Object.prototype.toString.call(xprint) === '[object Object]') {
                                console.log(val.macAddress+'/'+xprint.macAddress);
                                if (val.macAddress == xprint.macAddress) {
                                    st = 'Connected';
                                    //window.plugins.starPrinter.checkStatus(xprint.name, function(error, result){
                                    //    if (error) {
                                    //        console.log(error);
                                    //        var dd = [];
                                    //        var x = 0;
                                    //        
                                    //        angular.forEach(log.data, function(val){
                                    //            if (xprint.name == val.name) {
                                    //                angular.extend(val, {status: 'Offline'});
                                    //            }
                                    //            
                                    //            dd[x] = val;
                                    //            x++;
                                    //        });
                                    //        
                                    //        $scope.printMsg = '';
                                    //        $scope.printResult = dd;
                                    //    } else {
                                    //        console.log(result);
                                    //        (result.offline) ? st = 'Offline' : st = 'Connected';
                                    //        
                                    //        $scope.printMsg = '';
                                    //        $scope.printResult = log.data;
                                    //    }
                                    //});
                                }
                            } else {
                                st = 'Online';
                            }
                            
                            angular.extend(printerList[i], {status: st});
                            
                            log['data'] = printerList;
                            i++;
                        })
                        
                        //if (xprint == '' || xprint == undefined) {
                            console.log(log.data);
                            $scope.printMsg = 'Available';
                            $scope.printResult = log.data;
                        //}
                    } else {
                        $scope.printMsg = 'Printer is not available';
                    }
                }
            });
        }, 200);
    }
    $scope.printSel = function(pr, log) {
        var dd = [];
        var x = 0;
        
        angular.forEach(log, function(val){
            if (val.name == pr.name) {
                angular.extend(val, {status: 'Connected'});
            }
            
            dd[x] = val;
            x++;
        });
        
        $scope.printResult = dd;
        sessionService.set('printSel', JSON.stringify(pr));
    }
    $scope.printTest = function() {
        dataPrint();
    }
    
    $scope.htitle = 'Back';
    
    if ($location.path() == '/setting') {
        $scope.ttitle = 'Setting';
        $scope.type = 1;
        $scope.content = {url1: 'templates/v1/setting/menu.html', url2: 'templates/v1/setting/support.html'};
    }
    
    if ($location.path() == '/tools') {
        $scope.ttitle = 'Tools';
        $scope.type = 2;
        $scope.content = {url1: 'templates/v1/setting/tools.html', url2: ''};
    }
    
    if ($location.path() == '/taxes-gratuity') {
        $scope.ttitle = 'Taxes & Gratuity';
        $scope.type = 2;
        $scope.content = {url1: 'templates/v1/setting/taxes-gratuity.html', url2: ''};
    }
    
    if ($location.path() == '/bluetooth') {
        $scope.ttitle = 'Bluetooth';
        $scope.type = 2;
        $scope.content = {url1: 'templates/v1/setting/bluetooth.html', url2: ''};
        $scope.bluetooth();
    }
    
    if ($location.path() == '/printer') {
        $scope.ttitle = 'Printer';
        $scope.type = 2;
        $scope.content = {url1: 'templates/v1/setting/printer.html', url2: ''};
        //$scope.printer({port: 'Bluetooth'});
    }
    
    if ($location.path() == '/support') {
        $scope.ttitle = 'Support';
        $scope.type = 2;
        $scope.content = {url1: 'templates/v1/setting/support.html', url2: ''};
    }
    
    $scope.set = {taxes: 0, gratuity: 0, taxgratuity: ''};
    $scope.upTaxgratuity = function(set) {
        console.log(set);
    }
    
    $scope.sPrinter = function(port) {
        return $scope.printer(port);
    }
    
    $scope.sBluetooth = function() {
        return $scope.bluetooth();
    };
    
    $scope.upSupport = function(sp) {
        console.log(sp);
    }
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    $scope.closeSide = function(val) {
        $mdSidenav(val).close()
    }
    
}])

.controller('printCtrl', ['$scope', '$location', 'sessionService', 'nmFormat', '$mdDialog', '$mdSidenav', '$q', 'receiptPrint', 'dataPrint', 'deviceDetector',
                          function($scope, $location, sessionService, nmFormat, $mdDialog, $mdSidenav, $q, receiptPrint, dataPrint, deviceDetector) {
    
    $scope.path = $location.path();
    $scope.sidebar = {url1: 'templates/v1/sidebar/menu.html'};
    
    var dtAcc = JSON.parse(sessionService.get('acc_css'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    var orders = JSON.parse(sessionService.get('print'));console.log(orders);
    if (orders == undefined) {
        var confirm = $mdDialog.confirm()
                        .title('Message')
                        .htmlContent('Order is not available')
                        .ariaLabel('Alert')
                        .targetEvent('')
                        .ok('OK')
                        .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            $location.path('cashier');
        });
    }
    
    var xprint = JSON.parse(sessionService.get('printSel'));
    if (xprint == undefined) {
        var confirm = $mdDialog.confirm()
                        .title('Message')
                        .htmlContent('Please select printer first')
                        .ariaLabel('Alert')
                        .targetEvent('')
                        .ok('OK')
                        .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            $location.path('printer');
        });
    }
    
    var dt = {user: dtAcc, order: orders};
    var doc = receiptPrint(dt); console.log(doc);
    
    if ($location.path() == '/print') {
        
        $scope.htitle = 'Test Print';
        $scope.type = 3;
        $scope.content = {url1: 'templates/v1/activity/print.html',
                          url2: 'templates/v1/activity/print.html'};
        $scope.outlet = JSON.parse(sessionService.get('outlet'));
        
        setTimeout(function(){
            dataPrint(doc);
        }, 500);
        
        // Order
        $scope.dt = orders;
        $scope.cart = orders.sales_receipt_items;
        
        // Format Number
        $scope.valFormat = function(val) {
            return nmFormat(val);
        }
        
        // Sub Total
        $scope.xsubtotal = function(type) {
            var val = 0;
            
            angular.forEach(orders.sales_receipt_items, function(values) {
                if (values.product_type == 'discount') {
                    val = val - (values.product_sales_price*values.qty);
                } else {
                    val = val + (values.product_sales_price*values.qty);
                }
            })
            
            if (type == 2) {
                if (orders.sales_receipt_disc_amount != undefined && orders.sales_receipt_disc_amount != 0) {
                    val = val - parseFloat(orders.sales_receipt_disc_amount);
                }
                
                if (orders.sales_receipt_tax_amount != undefined && orders.sales_receipt_tax_amount != 0) {
		    val = val + parseFloat(orders.sales_receipt_tax_amount);
		}
            }
            
            if (type == 3) {
                if (orders.sales_receipt_disc_amount != undefined && orders.sales_receipt_disc_amount != 0) {
                    val = val - parseFloat(orders.sales_receipt_disc_amount);
                }
                
                if (orders.sales_receipt_tax_amount != undefined && orders.sales_receipt_tax_amount != 0) {
		    val = val + parseFloat(orders.sales_receipt_tax_amount);
		}
                
                if (orders.sales_receipt_charges_amount != undefined && orders.sales_receipt_charges_amount != 0) {
                    val = val + parseFloat(orders.sales_receipt_charges_amount);
                }
            }
            
            if (type > 0) {
                return val;
            } else {
                return nmFormat(val);
            }
        }
        $scope.subtotal = $scope.xsubtotal(0);
        
        // Grand Total
        $scope.total = function(type) {
            return nmFormat($scope.xsubtotal(type));
        }
        
        // Cash
        $scope.cash = nmFormat(orders.sales_receipt_paid);
        
        // Change
        $scope.change = function() {
            var change = nmFormat(parseFloat(orders.sales_receipt_paid) - $scope.xsubtotal(2));
            
            if (change == '') {
                return 0;
            } else {
                return change;
            }
        }
        
        $scope.openSide = function(val) {
            $mdSidenav(val).toggle()
        }
        $scope.closeSide = function(val) {
            $mdSidenav(val).close()
        }
        
    }
}])

