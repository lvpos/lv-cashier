(function() {
    'use strict';
    angular.module('ss.printer', [])
    .factory('DatecsPrinter', ['$q', '$timeout', '$window', '$mdDialog', DatecsPrinter]);

    function DatecsPrinter($q, $timeout, $window, $mdDialog) {
        if (!$window.cordova && !$window.DatecsPrinter) {
            console.log("DatecsPrinter plugin is missing. Have you installed the plugin? \nRun 'cordova plugin add cordova-plugin-datecs-printer'");
        }
        
        var cashDrawer = function(cashcut) {
            var xprint = JSON.parse(localStorage.getItem('printSel'));
            if (cashcut == undefined) {
                console.log('Cut Paper');
                window.plugins.starPrinter.cutPaper(xprint.name, function(error, result){
                    if (error) {
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Failed')
                            .htmlContent('<p>Failed to connect printer</p>')
                            .ariaLabel('Print')
                            .ok('OK')
                            .targetEvent(null)
                        );
                    }
                });
            } else {
                console.log('Cash Drawer & Cut Paper');
                window.plugins.starPrinter.openCashCut(xprint.name, function(error, result){
                    if (error) {
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Failed')
                            .htmlContent('<p>Failed to connect printer</p>')
                            .ariaLabel('Print')
                            .ok('OK')
                            .targetEvent(null)
                        );
                    }
                });
            }
        }
        
        var defaultTimeout = 100;
    
        var listBluetoothDevices = function () {
            console.log(1);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.listBluetoothDevices(
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var connect = function (device) {
            console.log(2);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.connect(
                    device.address,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var disconnect = function (cashcut) {
            console.log(3);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.disconnect(
                    function (success) {
                        cashDrawer(cashcut);
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var feedPaper = function (lines) {
            console.log(4);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.feedPaper(
                    lines,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var printText = function (text, charset) {
            console.log(5);
            var deferred = $q.defer();
            
            if (charset == null) {
                charset = 'ISO-8859-1';
            }
            
            $timeout(function() {
                $window.DatecsPrinter.printText(
                    text,
                    charset,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                ); 
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var setBarcode = function (align, small, scale, hri, height) {
            console.log(6);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.setBarcode(
                    align,
                    small,
                    scale,
                    hri,
                    height,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var printBarcode = function (type, data) {
            console.log(7);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.printBarcode(
                    type,
                    data,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        var getStatus = function () {
            console.log(8);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.getStatus(
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, 10);
            
            return deferred.promise;
        };
    
        var getTemperature = function () {
            console.log(9);
            var deferred = $q.defer();
            
            $timeout(function() {
                $window.DatecsPrinter.getTemperature(
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
      
            return deferred.promise;
        };
    
        var printImage = function (image, height, width, align) {
            console.log(10);
            var deferred = $q.defer();
            
            if (align == null) {
                align = 0;
            }
            
            $timeout(function() {
                $window.DatecsPrinter.printImage(
                    image,
                    height,
                    width,
                    align,
                    function (success) {
                        deferred.resolve(success);
                    },
                    function (error) {
                        deferred.reject(error);
                    }
                );
            }, defaultTimeout);
            
            return deferred.promise;
        };
    
        return {
            listBluetoothDevices: listBluetoothDevices,
            connect: connect,
            disconnect: disconnect,
            feedPaper: feedPaper,
            printText: printText,
            setBarcode: setBarcode,
            printBarcode: printBarcode,
            getStatus: getStatus,
            getTemperature: getTemperature,
            printImage: printImage
        }
    }
})();