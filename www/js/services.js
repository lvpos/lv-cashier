
angular.module('ss.services', [])

.factory('Scanner', ['$rootScope', function($rootScope) {
    return {
	onScan: function onScan() {
	    var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	    
	    var barcodePrefix = _ref.barcodePrefix;
	    var barcodeLength = _ref.barcodeLength;
	    var scanDuration = _ref.scanDuration;
	    var scanHandler = arguments[1];
	    
	    if (typeof barcodePrefix !== 'string') {
		throw new TypeError('barcodePrefix must be a string');
	    }
	    if (barcodeLength && typeof barcodeLength !== 'number') {
		throw new TypeError('barcodeLength must be a number');
	    }
	    if (scanDuration && typeof scanDuration !== 'number') {
		throw new TypeError('scanDuration must be a number');
	    }
	    if (typeof scanHandler !== 'function') {
		throw new TypeError('scanHandler must be a function');
	    }
	
	    /**
	     * SwipeTrack calls this function, if defined, whenever a barcode is scanned
	     * within the SwipeTrack browser.  See "SwipeTrack Browser JavaScript Functions" section of
	     * SwipeTrack API: http://swipetrack.net/support/faq/pdf/SwipeTrack%20API%20(v5.0.0).pdf
	    */
	    if (typeof window.onScanAppBarCodeData !== 'function') {
		window.onScanAppBarCodeData = function (barcode) {
		    window.onScanAppBarCodeData.scanHandlers.forEach(function (handler) {
			return handler(barcode);
		    });
		    return true;
		};
		window.onScanAppBarCodeData.scanHandlers = [];
	    }
	    var swipeTrackHandler = function swipeTrackHandler(barcode) {
		if (barcode.match('^' + barcodePrefix) !== null) scanHandler(barcode.slice(barcodePrefix.length));
	    };
	    window.onScanAppBarCodeData.scanHandlers.push(swipeTrackHandler);
	
	    scanDuration = scanDuration || 50;
	    var resScanner = '';
	    var isScanning = false;
	    var codeBuffer = '';
	    var scannedPrefix = '';
	    var finishScan = function finishScan() {
		if (codeBuffer) {
		    if (!barcodeLength) scanHandler(codeBuffer);else if (codeBuffer.length >= barcodeLength) scanHandler(codeBuffer.substr(0, barcodeLength));
		}
		scannedPrefix = '';
		codeBuffer = '';
		isScanning = false;
	    };
	    var keypressHandler = function keypressHandler(e) {
		var wait = $(this).data('wait');
		if (wait) {
		    clearTimeout(wait);
		}
		
		wait = setTimeout(function() {
		    var char = String.fromCharCode(e.which);
		    var charIndex = barcodePrefix.indexOf(char);
		    var expectedPrefix = barcodePrefix.slice(0, charIndex);
		    if (!isScanning) {
			isScanning = true;
			setTimeout(finishScan, scanDuration);
		    }
		    console.log(scannedPrefix+'/'+barcodePrefix);
		    if (scannedPrefix === barcodePrefix && /[^\s]/.test(char)) {
			console.log(1);
			codeBuffer += char;
			resScanner = codeBuffer;
		    } else if (scannedPrefix === expectedPrefix && char === barcodePrefix.charAt(charIndex)) {
			console.log(2);
			scannedPrefix += char;
			resScanner = scannedPrefix;
		    }
		    
		    $rootScope.$broadcast("hidScanner::scanned", {barcode: resScanner});
		}, 500)
		
		$(this).data('wait', wait);
	        return false;
	    };
	    var removeListener = function removeListener() {
		document.removeEventListener('keypress', keypressHandler);
		var swipeTrackHandlerIndex = window.onScanAppBarCodeData.scanHandlers.indexOf(swipeTrackHandler);
		if (swipeTrackHandlerIndex >= 0) window.onScanAppBarCodeData.scanHandlers.splice(swipeTrackHandlerIndex, 1);
	    };
	    document.addEventListener('keypress', keypressHandler);
	    return removeListener;
	}
	//check : function() {
	//    $ionicLoading.hide();
	//    return $http({ method: 'HEAD', url: Projects.zUrl + "?rand=" + Math.floor((1 + Math.random()) * 0x10000) })
	//    .then(function(response) {
	//	console.log(response);
	//    }, function(err) {
	//	console.log(err);
	//    });
	//}
    }
}])

.factory('AuthenticationService', ['$http', '$rootScope', 'base64', function($http, $rootScope, base64) {
    var service = {};
    
    service.SetCredentials = function(username, password) {
        var authdata = base64.urlencode(username + ':' + password);
	
	//$http.defaults.headers.common = {"Access-Control-Request-Headers": "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method"};
    	//$http.defaults.headers.common = {"Access-Control-Allow-Method": "GET, POST, OPTIONS, PUT, DELETE"};
        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
    };

    service.ClearCredentials = function () {
        $rootScope.globals = {};
        $http.defaults.headers.common.Authorization = 'Basic ';
    };

    return service;
}])

.factory('formDataObject', function() {
    return function(data) {
        var fd = new FormData();
        angular.forEach(data, function(value, key) {
	    if(Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
            {
		angular.forEach(value, function(v1, k1) {
		    fd.append(key+"["+k1+"]", v1);
		    if(Object.prototype.toString.call(v1) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
		    {
			angular.forEach(v1, function(v2, k2) {
			    fd.append(key+"["+k1+"]["+k2+"]", v2);
			});
		    }
		});
	    } else {
		fd.append(key, value);
	    }
        });
        return fd;
    };
})

.factory('Projects', ['$http', '$rootScope', '$ionicLoading', '$location', '$mdDialog', 'formDataObject',
		      function($http, $rootScope, $ionicLoading, $location, $mdDialog, formDataObject) {
    return {
    	curlGet: function(weburl, uri, xdata) {
	    return $http.get(weburl+uri, xdata);
    	},
    	curlPost: function(weburl, uri, xdata, ev) {
	    return $http.post(weburl+uri, xdata)
	    .success(function(data, status, headers, config) {
		console.log(data);
	    })
	    .error(function(data, status, headers, config) {
		console.log(data);
		$ionicLoading.hide();
		if (data != undefined && data.error.message != undefined) {
		    $mdDialog.hide();
		    var confirm = $mdDialog.confirm()
				    .title('Message')
				    .htmlContent(data.error.message)
				    .ariaLabel('Alert')
				    .targetEvent('')
				    .ok('OK')
				    .cancel('Cancel');
		    $mdDialog.show(confirm).then(function() {
			if (ev != undefined) {
			    ev(null, xdata);
			}
		    });
		}
		
		if (data != undefined && data.error.log != undefined && data.error.log > 0) {
		    localStorage.removeItem('uid_css');
		    localStorage.removeItem('acc_css');
		    localStorage.removeItem('cart');
		    $location.path('login');
		}
	    });
    	},
	curlMulti: function(weburl, uri, xdata, ev) {
	    return $http.post(weburl+uri, formDataObject(xdata), {
		transformRequest: angular.identity,
		headers: {'Content-Type': undefined}
	    })
	    .success(function(data, status, headers, config) {
		console.log(data);
	    })
	    .error(function(data, status, headers, config) {
		console.log(data);
		$ionicLoading.hide();
		if (data != undefined && data.error.message != undefined) {
		    $mdDialog.hide();
		    var confirm = $mdDialog.confirm()
				    .title('Message')
				    .htmlContent(data.error.message)
				    .ariaLabel('Alert')
				    .targetEvent('')
				    .ok('OK')
				    .cancel('Cancel');
		    $mdDialog.show(confirm).then(function() {
			if (ev != undefined) {
			    ev(null, xdata);
			}
		    });
		}
		
		if (data != undefined && data.error.log != undefined && data.error.log > 0) {
		    localStorage.removeItem('uid_css');
		    localStorage.removeItem('acc_css');
		    localStorage.removeItem('cart');
		    $location.path('login');
		}
	    });
    	},
	zUrl: 'http://lvpos.sambelpedes.com/webcode/',
	//zUrl: 'http://localhost:8081/project/aplikasi/lvpos/webcode/',
	zImg: 'http://lvpos.sambelpedes.com/webcode/storage/images/',
	zDoo: '',
	zKoo: '',
	ss: {user_id: '1', user_role_id: '1', menu_id: '1', session_key: 'cfY4DblviObco7MZp5or13Z0z8dwa7DBPhIDTsktc3NmFrsebEl9Iux1CMwC', ignoreLoadingBar: true}
    }
}])

.factory('queryExec', ['$cordovaSQLite', function($cordovaSQLite) {
    return {
	getData: function(key) {
	    var query = "SELECT data_name, data_value FROM css_data WHERE data_name = ?";
	    $cordovaSQLite.execute(db, query, [data_name]).then(function(res) {
		if(res.rows.length > 0) {
		    console.log("SELECTED -> " + res.rows.item(0).data_name + " " + res.rows.item(0).data_value);
		} else {
		    console.log("No results found");
		}
	    }, function (err) {
		console.error(err);
	    });
	},
	insData: function(key) {
	    var query = "INSERT INTO css_data (data_name, data_value) VALUES (?,?)";
	    return $cordovaSQLite.execute(db, query, [data_name, data_value]).then(function(res) {
		console.log("INSERT ID -> " + res.insertId);
	    }, function (err) {
		console.error(err);
	    });
	},
	upData: function(key) {
	    var query = "UPDATE css_data SET data_name=?, data_value=? WHERE data_name=?";
	    return $cordovaSQLite.execute(db, query, [data_name, data_value, data_name]).then(function(res) {
		console.log("UPDATE ID -> " + res.updateId);
	    }, function (err) {
		console.error(err);
	    });
	},
	delData: function(key) {
	    var query = "DELETE FROM css_data WHERE data_name=?";
	    return $cordovaSQLite.execute(db, query, [data_name]).then(function(res) {
		console.log("DELETE Successfully");
	    }, function (err) {
		console.error(err);
	    });
	},
	dropData: function(key) {
	    var query = "DROP TABLE css_data";
	    return $cordovaSQLite.execute(db, query).then(function(res) {
		console.log("DROP Successfully");
	    }, function (err) {
		console.error(err);
	    });
	}
    }
}])

.factory('sessionService', ['Projects', 'AuthenticationService', function(Projects, AuthenticationService) {
    return {
	set: function(key, value) {
	    return localStorage.setItem(key, value);
	},
	get: function(key) {
	    return localStorage.getItem(key);
	},
	destroy: function(key) {
	    return localStorage.removeItem(key);
	},
	checklog: function() {
	    //AuthenticationService.SetCredentials(zDoor, zKey);
	    //return Projects.curlGet(urlAPI, 'agent/checksession')
	}
    }
}])

.factory('dateTime', function() {
    return {
	// For todays date;
	today: function () { 
	    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
	},
	// For the time now
	timenow: function () {
	    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
	}
    }
})

.factory('nmFormat', function() {
    return function(input, type) {
	if(isNaN(input))return"";
	input = input*1;
	var str = new String(input);
	var result = "", len = str.length;
	for(var i=len-1;i>=0;i--)
	{
	    if((i+1)%3 == 0 && i+1!=len)result+=".";
	    result+=str.charAt(len-1-i);
	}
	return result;
    }
})

.factory('summaryData', function() {
    return function(dt) {
	var tdate = '';
	var tsales = 0;
	var tgross = 0;
	var tdiscitem = 0;
	var tchargeitem = 0;
	var tdiscrcp = 0;
	var tchargercp = 0;
	var ttax = 0;
	
	if (dt[0] != undefined) {
	    var orders = dt[0].sales_data;
	    var countdd = orders.length;
	    var tdate = orders[0].sales_receipt_date;
	    
	    // Subtotal
	    var subtotal = function(dt, type) {
		var val = 0;
		
		angular.forEach(dt.sales_receipt_items, function(values) {
		    if (values.product_type == 'discount') {
			val = val - (values.product_sales_price*values.qty);
		    } else {
			val = val + (values.product_sales_price*values.qty);
		    }
		})
		
		if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
		    val = val - parseFloat(dt.sales_receipt_disc_amount);
		}
		
		if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
		    val = val + parseFloat(dt.sales_receipt_tax_amount);
		}
		
		if (dt.sales_receipt_charges_amount != undefined && dt.sales_receipt_charges_amount != 0) {
		    val = val + parseFloat(dt.sales_receipt_charges_amount);
		}
		
		if (type > 0) {
		    return val;
		} else {
		    return nmFormat(val);
		}
	    }
	    
	    // Total Sales
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    nprice = subtotal(dt, 1)
		    tsales = tsales + nprice;
		}
	    })
	    
	    // Gross Sales
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    angular.forEach(dt.sales_receipt_items, function(val){
			if (val.product_type == 'item') {
			    tgross = tgross + (val.product_sales_price*val.qty);
			}
		    })
		}
	    })
	    
	    // Discount By Item
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    angular.forEach(dt.sales_receipt_items, function(val){
			if (val.product_type == 'discount') {
			    tdiscitem = tdiscitem + (val.product_sales_price*val.qty);
			}
		    })
		}
	    })
	    
	    // Charge By Item
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    angular.forEach(dt.sales_receipt_items, function(val){
			if (val.product_type == 'charge') {
			    tchargeitem = tchargeitem + (val.product_sales_price*val.qty);
			}
		    })
		}
	    })
	    
	    // Discount By Receipt
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
			tdiscrcp = tdiscrcp + parseFloat(dt.sales_receipt_disc_amount);
		    }
		}
	    })
	    
	    // Charge By Receipt
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    if (dt.sales_receipt_charges_amount != undefined && dt.sales_receipt_charges_amount != 0) {
			tchargercp = tchargercp + parseFloat(dt.sales_receipt_charges_amount);
		    }
		}
	    })
	    
	    // Tax
	    angular.forEach(orders, function(dt) {
		if (dt.sales_receipt_status == 'done') {
		    if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
			ttax = ttax + parseFloat(dt.sales_receipt_tax_amount);
		    }
		}
	    })
	}
	
	var summary = {sales_date: tdate, total_sales: tsales, gross_sales: tgross, tax: ttax,
		       discount_by_item: tdiscitem, charge_by_item: tchargeitem,
		       discount_by_receipt: tdiscrcp, charge_by_receipt: tchargercp};
	
	return summary;
    }
})

.factory('dataPrint', ['DatecsPrinter', 'sessionService', '$mdDialog', 'deviceDetector', 'webPrinter',
		       function(DatecsPrinter, sessionService, $mdDialog, deviceDetector, webPrinter) {
    return function(doc, cashcut) {
	var dvdetect = deviceDetector;
	if (dvdetect.os == 'android' || dvdetect.os == 'ios' || dvdetect.os == 'windows-phone') {
	    var blueth = sessionService.get('enableblue');
	    if (blueth == 'false') {
		$mdDialog.show(
		    $mdDialog.alert()
		    .parent(angular.element(document.body))
		    .clickOutsideToClose(true)
		    .title('Failed')
		    .htmlContent('<p>Please turn on your bluetooth</p>')
		    .ariaLabel('Print')
		    .ok('OK')
		    .targetEvent(null)
		);
	    } else {
		var xprint = JSON.parse(sessionService.get('printSel'));
		if (doc == undefined) {
		    doc = 'Test Printer';
		}
		
		if (Object.prototype.toString.call(xprint) === '[object Object]') {
		    var xpname = xprint.name.split(':');
		    var device = {address: xprint.macAddress, name: xpname[1], Type: 1}; console.log(device);
		    
		    if (xpname[0] == 'BT') {
			DatecsPrinter.connect(device)
			.then(function(success) {
			    console.log(success);
			    return DatecsPrinter.printText(doc);
			})
			.then(function(success) {
			    console.log(success);
			    return DatecsPrinter.feedPaper(10);
			})
			.then(function(success) {
			    console.log(success);
			    return DatecsPrinter.disconnect(cashcut);
			})
			.catch(function (error) {
			    $mdDialog.show(
				$mdDialog.alert()
				.parent(angular.element(document.body))
				.clickOutsideToClose(true)
				.title('Failed')
				.htmlContent('<p>Failed to connect printer</p>')
				.ariaLabel('Print')
				.ok('OK')
				.targetEvent(null)
			    );
			});
		    } else {
			console.log('Printer StarMpop');
			window.plugins.starPrinter.printReceipt(xprint.name, doc, function(error, result){
			    if (error) {
				$mdDialog.show(
				    $mdDialog.alert()
				    .parent(angular.element(document.body))
				    .clickOutsideToClose(true)
				    .title('Failed')
				    .htmlContent('<p>Failed to connect printer</p>')
				    .ariaLabel('Print')
				    .ok('OK')
				    .targetEvent(null)
				);
			    } else {
				console.log("Success");
			    }
			});
		    }
		} else {
		    console.log('Select Printer');
		    $mdDialog.show(
			$mdDialog.alert()
			.parent(angular.element(document.body))
			.clickOutsideToClose(true)
			.title('Failed')
			.htmlContent('<p>Please select printer first</p>')
			.ariaLabel('Print')
			.ok('OK')
			.targetEvent(null)
		    );
		}
	    }
	} else {
	    webPrinter.printHtml(doc);
	}
    }
}])

.factory('formatPrint', ['deviceDetector', function(deviceDetector) {
    return function(val, type, sub) {
	var space = " ";
	var xx = '';
	var lg = 0;
	
	var dvc = 0;
	var dvdetect = deviceDetector;
	if (dvdetect.os == 'android' || dvdetect.os == 'ios' || dvdetect.os == 'windows-phone') {
	    var dvc = 1;
	    var cline = 32;
	    var space = " ";
	    var xenter = "\r\n";
	}
	
	if (type > 0) {
	    if (sub.length > 0) {
		spl = val.split(";");
		if (spl.length > 1) {
		    // Data with Delimiter
		    for(i=0;i<spl.length;i++)
		    {
			if (i == 0) {
			    cc = cline - (sub.length + spl[i].length);
			    for(x=0;x<cc;x++)
			    {
				xx+= space;
			    }
			    if (dvc > 0) {
				xx+= $.trim(spl[i])+xenter;
			    } else {
				xx+= "<div style='float: right; width: 50%; text-align: right;'>"+spl[i]+"</div>";
			    }
			} else {
			    lg = spl[i].length;
			    cc = cline-lg;
			    for(x=0;x<cc;x++)
			    {
				xx+= space;
			    }
			    if (dvc > 0) {
				xx+= $.trim(spl[i])+xenter;
			    } else {
				xx+= "<div style='text-align: right;'>"+spl[i]+"</div>";
			    }
			}
		    }
		} else {
		    lg = val.length;
		    cc = cline-lg;
		    
		    if (cc > 0) {
			cc = cc/2;
			hc = cline - (sub.length + val.length + cc);
			cc = cc + hc;
			
			for(i=0;i<cc;i++)
			{
			    xx+= space;
			}
			xx+= $.trim(val)+xenter;
		    } else {
			if (dvc > 0) {
			    xx+= $.trim(val)+xenter;
			} else {
			    xx+= "<div style='float: right; width: 50%; text-align: right;'>"+val+"</div><div style='clear: both;'></div>";
			}
		    }
		}
	    } else {
		if (dvc > 0) {
		    xx+= $.trim(val);
		} else {
		    xx+= "<div style='float: left; width: 50%;'>"+val+"</div>";
		}
	    }
	} else {
	    spl = val.split("\r\n");
	    if (spl.length > 1) {
		// Data with Enter \r\n
		for(i=0;i<spl.length;i++)
		{
		    lg = spl[i].length;
		    cc = cline-lg;
		    
		    if (cc > 0) {
			if (dvc > 0) {
			    cc = cc/2;
			    for(x=0;x<cc;x++)
			    {
				xx+= space;
			    }
			    xx+= $.trim(spl[i])+xenter;
			} else {
			    xx+= "<div style='text-align: center;'>"+spl[i]+"</div>";
			}
		    } else {
			if (dvc > 0) {
			    xx+= $.trim(spl[i])+xenter;
			} else {
			    xx+= "<div style='text-align: center;'>"+spl[i]+"</div>";
			}
		    } 
		}
	    } else {
		if (dvc > 0) {
		    // Data Normal
		    lg = val.length;
		    cc = cline-lg;
		    
		    if (cc > 0) {
			cc = cc/2;
			for(i=0;i<cc;i++)
			{
			    xx+= space;
			}
			xx+= $.trim(val)+xenter;
		    } else {
			xx+= $.trim(val)+xenter;
		    }
		} else {
		    if (val == '--------------------------------' || val == '________________________________') {
			xx+= "<hr>";
		    } else {
			xx+= "<div style='text-align: center;'>"+val+"</div>";
		    }
		}
	    }
	}
	
	return xx;
    }
}])

.factory('receiptPrint', ['sessionService', 'formatPrint', 'nmFormat', function(sessionService, formatPrint, nmFormat) {
    return function(dt, trial) {
        // Sub Total
        var xsubtotal = function(type) {
            var val = 0;
            
            angular.forEach(dt.order.sales_receipt_items, function(values) {
		if (values.product_type == 'discount') {
                    val = val - (values.product_sales_price*values.qty);
                } else {
                    val = val + (values.product_sales_price*values.qty);
                }
            })
            
	    if (type == 2) {
		if (dt.order.sales_receipt_disc_amount != undefined && dt.order.sales_receipt_disc_amount != 0) {
		    val = val - parseFloat(dt.order.sales_receipt_disc_amount);
		}
		
		if (dt.order.sales_receipt_tax_amount != undefined && dt.order.sales_receipt_tax_amount != 0) {
		    val = val + parseFloat(dt.order.sales_receipt_tax_amount);
		}
	    }
	    
	    if (type == 3) {
		if (dt.order.sales_receipt_disc_amount != undefined && dt.order.sales_receipt_disc_amount != 0) {
		    val = val - parseFloat(dt.order.sales_receipt_disc_amount);
		}
		
		if (dt.order.sales_receipt_tax_amount != undefined && dt.order.sales_receipt_tax_amount != 0) {
		    val = val + parseFloat(dt.order.sales_receipt_tax_amount);
		}
		
		if (dt.order.sales_receipt_charges_amount != undefined && dt.order.sales_receipt_charges_amount != 0) {
		    val = val + parseFloat(dt.order.sales_receipt_charges_amount);
		}
	    }
	    
            if (type > 0) {
                return val;
            } else {
                return nmFormat(val);
            }
        }
        var subtotal = xsubtotal(0);
        
        // Grand Total
        var total = function(type) {
            return nmFormat(xsubtotal(type));
        }
        
        // Cash
        var cash = nmFormat(dt.order.sales_receipt_paid);
        
        // Change
        var change = function() {
            var change = nmFormat(parseFloat(dt.order.sales_receipt_paid) - xsubtotal(2));
            
            if (change == '') {
                return 0;
            } else {
                return change;
            }
        }
	
	var outlet = JSON.parse(sessionService.get('outlet'));
	
	var d = new Date(dt.order.sales_receipt_date);
	var month = ["January","Feburary","March","April","May","June","July","August","September","October","November","December"];
	var xdate = d.getDate()+' '+month[d.getMonth()]+' '+d.getFullYear();
	var xtime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
	
	var i = 0;
	var space = " ";
	var line = "--------------------------------";
	var doc = [];
	doc[i] = outlet.outlet_name; i++;
	doc[i] = outlet.outlet_info.outlet_address; i++;
	doc[i] = outlet.outlet_info.outlet_phone; i++;
	doc[i] = line; i++;
	doc[i] = [xdate, xtime]; i++;
	doc[i] = ["Receipt No", dt.order.sales_receipt_no]; i++;
	
	if (dt.user != undefined && dt.user.user_fname != undefined) {
	    doc[i] = ["Collected By", dt.user.user_fname+";"+dt.user.user_lname]; i++;
	} else {
	    doc[i] = ["Collected By", dt.order.sales_receipt_person.name]; i++;
	}
	
	doc[i] = line; i++;
	
	angular.forEach(dt.order.sales_receipt_items, function(val){
	    pname = val.product_name;
	    if (pname.length > 30) {
		//pname = pname.substring(0, 14)+".. x"+val.qty;
		pname = pname.substring(0, 30)+"..";
	    } else {
		//pname = pname+" x"+val.qty;
		pname = pname;
	    }
	    
	    sprice = nmFormat(1*val.product_sales_price);
	    sprice = parseFloat(sprice.replace(/[^\w\s]/gi, '')); 
	    pprice = nmFormat(val.product_sales_price*val.qty);
	    
	    if (val.product_type == 'discount') {
		pprice = "- Rp "+pprice;
	    } else {
		pprice = "Rp "+pprice;
	    }
	    
	    doc[i] = [pname, '']; i++;
	    doc[i] = [val.qty+"x"+sprice, pprice]; i++;
	})
	
	doc[i] = line; i++;
	doc[i] = ["Subtotal", "Rp "+subtotal]; i++;
	
	if (dt.order.sales_receipt_disc_amount != undefined && dt.order.sales_receipt_disc_amount != 0) {
	    doc[i] = ["Disc", "Rp "+nmFormat(dt.order.sales_receipt_disc_amount)]; i++;
	}
	
	if (dt.order.sales_receipt_tax_amount != undefined && dt.order.sales_receipt_tax_amount != 0) {
	    doc[i] = ["Tax (PPN "+dt.order.sales_receipt_tax+"%)", "Rp "+nmFormat(dt.order.sales_receipt_tax_amount)]; i++;
	}
	
	doc[i] = line; i++;
	doc[i] = ["Grand Total", "Rp "+total(2)]; i++;
	
	if (dt.order.sales_receipt_charges_amount != undefined && dt.order.sales_receipt_charges_amount != 0) {
	    doc[i] = ["Charge", "Rp "+nmFormat(dt.order.sales_receipt_charges_amount)]; i++;
	}
	
	doc[i] = line; i++;
	doc[i] = ["Total Payment", "Rp "+total(3)]; i++;
	
	if (trial == undefined) {
	    doc[i] = ["Cash", "Rp "+cash]; i++;
	    doc[i] = ["Change", "Rp "+change()]; i++;
	}
	
	//doc[i] = line; i++;
	//doc[i] = "Thank you for shopping, www.logivar.com, cs@logivar.com"; i++;
	doc[i] = "\r\n"; i++;
	
	var xx = '';
	angular.forEach(doc, function(val){
	    if (Object.prototype.toString.call(val) === '[object Array]') {
		var dt = '';
		angular.forEach(val, function(v){
		    xx+= formatPrint(v, 1, dt);
		    dt = v;
		})
	    } else {
		xx+= formatPrint(val);
	    }
	    
	    return xx;
	})
	
	return xx;
    }
}])

.factory('summaryPrint', ['sessionService', 'formatPrint', 'nmFormat', '$filter', function(sessionService, formatPrint, nmFormat, $filter) {
    return function(dt) {
	var orders = dt[0].sales_data;
	var countdd = orders.length;
	var outlet = JSON.parse(sessionService.get('outlet'));
	
	// Subtotal
	var subtotal = function(dt, type) {
	    var val = 0;
	    
	    angular.forEach(dt.sales_receipt_items, function(values) {
		if (values.product_type == 'discount') {
		    val = val - (values.product_sales_price*values.qty);
		} else {
		    val = val + (values.product_sales_price*values.qty);
		}
	    })
	    
	    if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
		val = val - parseFloat(dt.sales_receipt_disc_amount);
	    }
	    
	    if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
		val = val + parseFloat(dt.sales_receipt_tax_amount);
	    }
	    
	    if (dt.sales_receipt_charges_amount != undefined && dt.sales_receipt_charges_amount != 0) {
		val = val + parseFloat(dt.sales_receipt_charges_amount);
	    }
	    
	    if (type > 0) {
		return val;
	    } else {
		return nmFormat(val);
	    }
	}
	
	var d = new Date();
	var month = ["January","Feburary","March","April","May","June","July","August","September","October","November","December"];
	var xdate = d.getDate()+' '+month[d.getMonth()]+' '+d.getFullYear();
	var xtime = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
	
	var i = 0;
	var space = " ";
	var line = "--------------------------------";
	var liner = "________________________________";
	var doc = [];
	doc[i] = outlet.outlet_name; i++;
	doc[i] = outlet.outlet_info.outlet_address; i++;
	doc[i] = outlet.outlet_info.outlet_phone; i++;
	doc[i] = line; i++;
	doc[i] = ['Daily Summary', dt[0].created_at]; i++;
	doc[i] = liner; i++;
	doc[i] = line; i++;
	
	doc[i] = "\r\n"; i++;
	doc[i] = ['Total Sales', '']; i++;
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	
	var tprice = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		nprice = subtotal(dt, 1)
		pprice = nmFormat(nprice);
		pprice = "Rp "+pprice;
		
		tprice = tprice + nprice;
		doc[i] = [dt.sales_receipt_no, pprice]; i++;
	    }
	})
	
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
	doc[i] = liner; i++;
	
	doc[i] = line; i++;
	
	doc[i] = "\r\n"; i++;
	doc[i] = ['Gross Sales', '']; i++;
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	
	var tprice = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		angular.forEach(dt.sales_receipt_items, function(val){
		    if (val.product_type == 'item') {
			pname = val.product_name;
			if (pname.length > 30) {
			    //pname = pname.substring(0, 14)+".. x"+val.qty;
			    pname = pname.substring(0, 30)+"..";
			} else {
			    //pname = pname+" x"+val.qty;
			    pname = pname;
			}
			
			sprice = nmFormat(1*val.product_sales_price);
			sprice = parseFloat(sprice.replace(/[^\w\s]/gi, '')); 
			pprice = nmFormat(val.product_sales_price*val.qty);
			
			pprice = "Rp "+pprice;
			tprice = tprice + (val.product_sales_price*val.qty);
			
			doc[i] = [pname, '']; i++;
			doc[i] = [val.qty+"x"+sprice, pprice]; i++;
		    }
		})
	    }
	})
	
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
	doc[i] = liner; i++;
	
	doc[i] = line; i++;
	
	doc[i] = "\r\n"; i++;
	doc[i] = ['Discount by Items', '']; i++;
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	
	var tprice = 0;
	var x = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		angular.forEach(dt.sales_receipt_items, function(val){
		    if (val.product_type == 'discount') {
			pname = val.product_name;
			if (pname.length > 30) {
			    //pname = pname.substring(0, 14)+".. x"+val.qty;
			    pname = pname.substring(0, 30)+"..";
			} else {
			    //pname = pname+" x"+val.qty;
			    pname = pname;
			}
			
			sprice = nmFormat(1*val.product_sales_price);
			sprice = parseFloat(sprice.replace(/[^\w\s]/gi, '')); 
			
			pprice = nmFormat(val.product_sales_price*val.qty);
			pprice = "Rp "+pprice;
			
			tprice = tprice + (val.product_sales_price*val.qty);
			
			doc[i] = [pname, '']; i++;
			doc[i] = [val.qty+"x"+sprice, pprice]; i++;
		    }
		})
	    }
	})
	
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
	doc[i] = liner; i++;
	
	doc[i] = line; i++;
	
	doc[i] = "\r\n"; i++;
	doc[i] = ['Charge by Items', '']; i++;
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	
	var tprice = 0;
	var x = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		angular.forEach(dt.sales_receipt_items, function(val){
		    if (val.product_type == 'charge') {
			pname = val.product_name;
			if (pname.length > 30) {
			    //pname = pname.substring(0, 14)+".. x"+val.qty;
			    pname = pname.substring(0, 30)+"..";
			} else {
			    //pname = pname+" x"+val.qty;
			    pname = pname;
			}
			
			sprice = nmFormat(1*val.product_sales_price);
			sprice = parseFloat(sprice.replace(/[^\w\s]/gi, '')); 
			
			pprice = nmFormat(val.product_sales_price*val.qty);
			pprice = "Rp "+pprice;
			
			tprice = tprice + (val.product_sales_price*val.qty);
			
			doc[i] = [pname, '']; i++;
			doc[i] = [val.qty+"x"+sprice, pprice]; i++;
		    }
		})
	    }
	})
	
	doc[i] = liner; i++;
	doc[i] = "\r\n"; i++;
	doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
	doc[i] = liner; i++;
	doc[i] = line; i++;
	
	var tprice = 0;
	var x = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		if (x == 0) {
		    doc[i] = "\r\n"; i++;
		    doc[i] = ['Discount by Receipt', '']; i++;
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		}
		
		if (dt.sales_receipt_disc_amount != undefined && dt.sales_receipt_disc_amount != 0) {
		    pprice = nmFormat(dt.sales_receipt_disc_amount);
		    pprice = "Rp "+pprice;
		    
		    tprice = tprice + parseFloat(dt.sales_receipt_disc_amount);
		    doc[i] = [dt.sales_receipt_no, pprice]; i++;
		}
		
		if (x == countdd-1) {
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		    doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
		    doc[i] = liner; i++;
		}
		
		x++;
	    }
	})
	
	doc[i] = line; i++;
	
	var tprice = 0;
	var x = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		if (x == 0) {
		    doc[i] = "\r\n"; i++;
		    doc[i] = ['Charge by Receipt', '']; i++;
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		}
		
		if (dt.sales_receipt_charges_amount != undefined && dt.sales_receipt_charges_amount != 0) {
		    pprice = nmFormat(dt.sales_receipt_charges_amount);
		    pprice = "Rp "+pprice;
		    
		    tprice = tprice + parseFloat(dt.sales_receipt_charges_amount);
		    doc[i] = [dt.sales_receipt_no, pprice]; i++;
		}
		
		if (x == countdd-1) {
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		    doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
		    doc[i] = liner; i++;
		}
		
		x++;
	    }
	})
	
	doc[i] = line; i++;
	
	var tprice = 0;
	var x = 0;
	angular.forEach(orders, function(dt) {
	    if (dt.sales_receipt_status == 'done') {
		if (x == 0) {
		    doc[i] = "\r\n"; i++;
		    doc[i] = ['Tax', '']; i++;
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		}
		
		if (dt.sales_receipt_tax_amount != undefined && dt.sales_receipt_tax_amount != 0) {
		    pprice = nmFormat(dt.sales_receipt_tax_amount);
		    pprice = "Rp "+pprice;
		    
		    tprice = tprice + parseFloat(dt.sales_receipt_tax_amount);
		    doc[i] = [dt.sales_receipt_no, pprice]; i++;
		}
		
		if (x == countdd-1) {
		    doc[i] = liner; i++;
		    doc[i] = "\r\n"; i++;
		    doc[i] = ["Total", "Rp "+nmFormat(tprice)]; i++;
		    doc[i] = liner; i++;
		}
		
		x++;
	    }
	})
	
	doc[i] = line; i++;
	
	var xx = '';
	angular.forEach(doc, function(val){
	    if (Object.prototype.toString.call(val) === '[object Array]') {
		var dt = '';
		angular.forEach(val, function(v){
		    xx+= formatPrint(v, 1, dt);
		    dt = v;
		})
	    } else {
		xx+= formatPrint(val);
	    }
	    
	    return xx;
	})
	
	return xx;
    }
}])

.factory('webPrinter', ['$rootScope', '$compile', '$http', '$timeout', '$q', function($rootScope, $compile, $http, $timeout, $q) {
    var printHtml = function (html) {
	var deferred = $q.defer();
	var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
	hiddenFrame.contentWindow.printAndRemove = function() {
	    hiddenFrame.contentWindow.print();
	    $(hiddenFrame).remove();
	};
	var htmlContent = "<!doctype html>"+
		    "<html>"+
			'<body onload="printAndRemove();">' +
			    html +
			'</body>'+
		    "</html>";
	var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
	doc.write(htmlContent);
	deferred.resolve();
	doc.close();
	return deferred.promise;
    };

    var openNewWindow = function (html) {
	var newWindow = window.open("printTest.html");
	newWindow.addEventListener('load', function(){ 
	    $(newWindow.document.body).html(html);
	}, false);
    };

    var print = function (templateUrl, data) {
	$http.get(templateUrl).success(function(template){
	    var printScope = $rootScope.$new()
	    angular.extend(printScope, data);
	    var element = $compile($('<div>' + template + '</div>'))(printScope);
	    var waitForRenderAndPrint = function() {
		if(printScope.$$phase || $http.pendingRequests.length) {
		    $timeout(waitForRenderAndPrint, 1000);
		} else {
		    // Replace printHtml with openNewWindow for debugging
		    printHtml(element.html());
		    printScope.$destroy();
		}
	    };
	    waitForRenderAndPrint();
	});
    };

    var printFromScope = function (templateUrl, scope) {
	$rootScope.isBeingPrinted = true;
	$http.get(templateUrl).success(function(template){
	    var printScope = scope;
	    var element = $compile($('<div>' + template + '</div>'))(printScope);
	    var waitForRenderAndPrint = function() {
		if (printScope.$$phase || $http.pendingRequests.length) {
		    $timeout(waitForRenderAndPrint);
		} else {
		    printHtml(element.html()).then(function() {
		       $rootScope.isBeingPrinted = false;
		   });

		}
	    };
	    waitForRenderAndPrint();
	});
    };
    return {
	print: print,
	printHtml: printHtml,
	openNewWindow: openNewWindow,
	printFromScope:printFromScope
    }
}]);

//$scope.printHtml = function(html, type) {
//    var deferred = $q.defer();
//    var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
//    hiddenFrame.contentWindow.printAndRemove = function() {
//        hiddenFrame.contentWindow.print();
//        $(hiddenFrame).remove();
//    };
//    
//    var htmlContent = "<!doctype html>"+
//                        "<html>"+
//                            '<body onload="printAndRemove();">' +
//                                html +
//                            '</body>'+
//                        "</html>";
//    
//    if (type > 0) {
//        // Print for Apps
//        return htmlContent;
//    } else {
//        // Print for Web
//        var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
//        doc.write(htmlContent);
//        deferred.resolve();
//        doc.close();
//        
//        return doc;
//    }
//};

//var doc = "\&#027; \&#32; \&#000;";           // Right Space
//doc += "\&#027; \&#029; \&#97; \&#001;";        // Align Center
//doc += "Your Store\r\n";
//doc += "Jalan Raya Bogor Km 100, Jawa Barat\r\n\r\n";
//doc += "\&#027; \&#029; \&#97; \&#000;";        // Align Left
//doc += "\&#027; \&#68; \&#002; \&#016; \&#000;";  //SetHT
//doc += xdate+"                "+xtime;
//doc += "\r\n";
//doc += "Receipt No"
//doc += "\&#009;";                          // HT
//doc += "\&#027; \&#029; \&#97; \&#002";        // Align Right
//doc += orders.sales_receipt_no+"\r\n";
//doc += "Collected"
//doc += "\&#009;";                          // HT
//doc += "\&#027; \&#029; \&#97; \&#002";        // Align Right
//doc += dtAcc.user_fname+" "+dtAcc.user_lname;

//var doc = "0x1b, 0x20, 0x00";           // Right Space
//doc += "0x1b, 0x1d, 0x61, 0x01";        // Align Center
//doc += "Your Store\r\n";
//doc += "Jalan Raya Bogor Km 100, Jawa Barat\r\n\r\n";
//doc += "0x1b, 0x1d, 0x61, 0x00";        // Align Left
//doc += "0x1b, 0x44, 0x02, 0x10, 0x00";  //SetHT
//doc += xdate+"                "+xtime;
//doc += "\r\n";
//doc += "Receipt No"
//doc += "0x09";                          // HT
//doc += "0x1b, 0x1d, 0x61, 0x02";        // Align Right
//doc += orders.sales_receipt_no+"\r\n";
//doc += "Collected"
//doc += "0x09";                          // HT
//doc += "0x1b, 0x1d, 0x61, 0x02";        // Align Right
//doc += dtAcc.user_fname+" "+dtAcc.user_lname;