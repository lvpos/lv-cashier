<div id='xprint'>
    <style>
        table {
            width: 100%;
            border: none;
            border-collapse: collapse;
            border-spacing: 0;
        }
        table td {
            padding: 3px;
            vertical-align: middle;
        }
        table .align-center {
            text-align: center;
        }
        table .align-right {
            text-align: right;
        }
        table .pad-tb-10 {
            padding: 10px 0;
        }
        table img {
            width: 15px;
            height: 15px;
            margin-right: 10px;
        }
    </style>
    <div style='padding: 10px'>
        <table>
            <tr class='align-center'>
                <td><h3>Logivar</h3></td>
            </tr>
            <tr class='align-center'>
                <td>Jalan Raya Bogor Km 100<br><i class='fa fa-phone fa-fw' aria-label='Phone'></i> 0987654321</td>
            </tr>
        </table>
        <hr>
        <table>
            <tr>
                <td style='width: 30%;'>Date</td><td class='align-right'>{{dt.sales_date | date:'dd MMM yyyy HH:mm:ss'}}</td>
            </tr>
            <tr>
                <td style='width: 30%;'>Receipt No</td><td class='align-right'>{{dt.sales_offline_no}}</td>
            </tr>
            <tr>
                <td style='width: 30%;'>Served By</td><td class='align-right'>{{accLog.user_fname}} {{accLog.user_lname}}</td>
            </tr>
            <tr>
                <td style='width: 30%;'>Collected By</td><td class='align-right'>{{accLog.user_fname}} {{accLog.user_lname}}</td>
            </tr>
        </table>
        <hr>
        <table>
            <tr ng-repeat='cc in cart'>
                <td style='width: 50%;'>{{cc.product_name}}<br>Rp {{valFormat(cc.product_sales_price*1)}}</td>
                <td style='width: 10%;' class='align-right'>x{{cc.qty}}</td>
                <td style='width: 40%;' class='align-right'>Rp {{valFormat(cc.product_sales_price*cc.qty)}}</td>
            </tr>
        </table>
        <hr>
        <table>
            <tr>
                <td style='width: 20%;'><h3>Total</h3></td><td class='align-right'><h3>Rp {{subtotal}}</h3></td>
            </tr>
            <tr>
                <td style='width: 20%;'>Cash</td><td class='align-right'>Rp {{cash}}</td>
            </tr>
            <tr>
                <td style='width: 20%;'>Change</td><td class='align-right'>Rp {{payChange()}}</td>
            </tr>
        </table>
        <hr>
        <table>
            <tr>
                <td><img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg'/><label>http://www.facebook.com</label></td>
            </tr>
            <tr>
                <td><img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg'/><label>http://www.facebook.com</label></td>
            </tr>
            <tr>
                <td><img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg'/><label>http://www.facebook.com</label></td>
            </tr>
            <tr>
                <td><img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg'/><label>http://www.facebook.com</label></td>
            </tr>
        </table>
        <hr>
        <table>
            <tr>
                <td><label>Notes</label><p>Test Notes</p></td>
            </tr>
        </table>
    </div>
</div>

<!--<md-content id='xprint' layout-padding>
    <div class='pad-tb-10 txl-center'>
        <h3>Logivar</h3>
        <span>Jalan Raya Bogor Km 100</span><br>
        <span>
            <i class='fa fa-phone fa-fw' aria-label='Phone'></i>
            <label>0987654321</label>
        </span>
    </div>
    <div class='bod-tb pad-tb-10'>
        <md-list-item class='noright min-hg mg-b-5'>
            <p flex='65'>Date</p>
            <label class='txl-right' flex='35'>{{dt.sales_date}}</label>
        </md-list-item>
        <md-list-item class='noright min-hg mg-b-5'>
            <p flex='65'>Receipt Number</p>
            <label class='txl-right' flex='35'>{{dt.sales_receipt_no}}</label>
        </md-list-item>
        <md-list-item class='noright min-hg mg-t-5 mg-b-5'>
            <p flex='65'>Served By</p>
            <label class='txl-right' flex='35'>{{accLog.user_fname}} {{accLog.user_lname}}</label>
        </md-list-item>
        <md-list-item class='noright min-hg mg-t-5 mg-b-5'>
            <p flex='65'>Collected By</p>
            <label class='txl-right' flex='35'>{{accLog.user_fname}} {{accLog.user_lname}}</label>
        </md-list-item>
    </div>
    <div class='bod-bottom pad-tb-10'>
        <div ng-repeat='cc in cart | filter:searchCart' class='cl-mg-res' layout='row'>
            <md-list-item class='noright min-hg mg-t-5 mg-b-5'>
                <p flex='55'>{{cc.product_name}}<br>Rp {{valFormat(cc.product_sales_price*1)}}</p>
                <label class='mg-lr-20' flex='10'>x{{cc.qty}}</label>
                <label class='txl-right' flex='35'>Rp {{valFormat(cc.product_sales_price*cc.qty)}}</label>
            </md-list-item>
        </div>
    </div>
    <div class='bod-bottom pad-tb-10'>
        <md-list-item class='noright min-hg mg-b-5'>
            <p class='ff-18' flex='65'>Total</p>
            <label class='txl-right ff-18' flex='35'>Rp {{subtotal}}</label>
        </md-list-item>
        <md-list-item class='noright min-hg mg-t-5 mg-b-5'>
            <p flex='65'>Cash</p>
            <label class='txl-right' flex='35'>Rp {{cash}}</label>
        </md-list-item>
        <md-list-item class='noright min-hg mg-t-5 mg-b-5'>
            <p flex='65'>Change</p>
            <label class='txl-right' flex='35'>Rp {{payChange()}}</label>
        </md-list-item>
    </div>
    <div class='bod-bottom pad-tb-10'>
        <label>Link</label>
        <md-list-item class='noright min-hg mg-b-5'>
            <img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg' class='md-avatar' />
            <p>www.logivar.com</p>
        </md-list-item>
        <md-list-item class='noright min-hg mg-b-5'>
            <img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg' class='md-avatar' />
            <p>www.facebook.com/logivar</p>
        </md-list-item>
        <md-list-item class='noright min-hg mg-b-5'>
            <img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg' class='md-avatar' />
            <p>insLogivar</p>
        </md-list-item>
        <md-list-item class='noright min-hg mg-b-5'>
            <img ng-src='https://material.angularjs.org/latest/img/icons/angular-logo.svg' class='md-avatar' />
            <p>twLogivar</p>
        </md-list-item>
    </div>
    <div class='pad-tb-10'>
        <label>Notes</label>
        <p>Print Test by Logivar</p>
    </div>
</md-content>-->